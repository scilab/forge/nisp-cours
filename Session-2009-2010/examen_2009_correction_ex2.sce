// Copyright (C) 2009 - CEA - Jean-Marc Martinez
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Mod�le math�matique � analyser : fonction produit
function y = Exemple (x)
  y=1;
  for i=1:nx
    y = y * x(i);
  end
endfunction

// Dimension 
d=5;

// On sp�cifie les moyennes et les variances des X(i)
mux=[1:d]; // mux(i)=i de i=1 � d
vax=[1:d]; // vax(i)=i de i=1 � d

// Calcul de pm et pv
pv=1;
pm=1;
for i=1:d
  pv = pv * ( vax(i) + mux(i)^2 );
  pm = pm * mux(i)^2;
end

// Calcul de la moyenne de Y
muy=1;
for i=1:d
  muy=muy*mux(i);
end

// Calcul de la variance de Y
vay = pv - pm;

// Calcul des indices du premier ordre
sx=ones(d,1);
for i=1:d
  sx(i) = vax(i) * pm / (mux(i)^2 * vay);
end

// Calcul des indices totaux
st=ones(d,1);
su=ones(d,1);
for i=1:d
  su(i) =  (pv * mux(i)^2)/(vax(i) + mux(i)^2) - pm;
end
for i=1:d
  st(i) = 1 - su(i)/vay;
end

nx=d;
srvu = setrandvar_new();
srvx = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Normale",mux(i),sqrt(vax(i)));
  setrandvar_addrandvar(srvu, rvu(i));
  rvx(i) = randvar_new("Normale");
  setrandvar_addrandvar(srvx, rvx(i));
end

// On v�rifie en �ditant sur la console les param�tres des variables X(i)
setrandvar_getlog(srvu);

// Specification d'un plan : quadrature tensoris�e
// formule excate pour un polyn�me de degr� = degre
degre = nx;
setrandvar_buildsample( srvx, "Quadrature", degre);
setrandvar_buildsample( srvu , srvx );


// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );

// R�alisation du plan d'exp�riences num�riques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata  = zeros(nx);
outputdata = zeros(ny);
for k=1:np
  inputdata  = setrandvar_getsample(srvu,k);
  outputdata = Exemple(inputdata);
  polychaos_settarget(pc,k,outputdata);
end

// Calcul des coefficients par int�gration num�rique
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");

// Edition de l'analyse de sensibilit�
mprintf("Nombre de simulations %d\n",setrandvar_getsize(srvx));
mprintf("Mean        = %f (expected %f) \n",polychaos_getmean(pc),muy);
mprintf("Variance    = %f (expected %f) \n",polychaos_getvariance(pc),vay);
mprintf("Indice de sensibilit� du 1er ordre\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindexfirst(pc,i),sx(i));
end
mprintf("Indice de sensibilite Totale\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindextotal(pc,i),st(i));
end


