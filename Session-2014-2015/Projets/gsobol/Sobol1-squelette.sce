// Mod�le math�matique : fonction gSobol
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    y = y * (abs(4*x(i)-2)+coef(i)) / (1 + coef(i));
  end
endfunction

// Dimension et coefficients alpha
d     = 3;
alpha = [0 1 2];


/////////////////////////////////////////////
// Valeurs calcul�es dans la partie th�orique
// 
// Calcul de la moyenne de Y
muy = TODO

// Calcul du carrre des coefficients de variation en fonction des alpha(i)
cv2 = TODO

// Calcul de la variance de Y en fonction des coefficients de variation
// et de la moyenne 
vay = TODO

// Calcul des indices de sensibilite en fonction des coefficients de variation
sx = TODO  // premier ordre 
sg = TODO  // sensibilite totale
//////////////////////////////////////////////


// Groupe de variables aleatoires
srvu = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Uniforme",0,1);
  setrandvar_addrandvar(srvu, rvu(i));
end
// On aurait pu definir plus directement le groupe de variables srvu
// srvu = setrandvar_new(d); // groupe de d variables uniformes [0,1]

// Realisation des 2 echantillons (tirages aleatoires Monte Carlo)
// Plans d'experiences : matrices A et B
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);
A = setrandvar_getsample(srvu);
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// Realisation des plans d'experiences A et B par appel au modele
for k=1:np
  ya(k) = MyFunction(A(k,:), alpha);
  yb(k) = MyFunction(B(k,:), alpha);
end

// Estimation des indices par la methode de Sobol
mprintf("\nSensitivity analysis\n"); // � completer ...
for i=1:d
  C=B; C(:,i) = A(:,i);
  s1 = TODO
  st = TODO
  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e - (expected %12.4e)\n", s1, sx(i));
  mprintf("Total index is : %12.4e - (expected %12.4e)\n", st, sg(i));
end
