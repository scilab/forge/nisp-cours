// Example of rejection sampling. 
// Reference
// Karl Sigman. Acceptance-rejection method, 2007. 
// Professor Karl Sigman’s Lecture Notes on Monte Carlo Simulation, 
// Columbia University, 
// http://www.columbia.edu/~ks20/4703-Sigman/4703-07-Notes-ARM.pdf.

// See how to generate standard normal random variables, 
// using a rejection algorithm 
// and a instrumental uniform distribution.

// 1. Get an approximate constant c
a=2;
b=5;
x = linspace(0.01,0.99,1000);
y = distfun_betapdf ( x , a, b);
plot(x,y)
xtitle("Beta distribution, a=2, b=5","X","Density")

// See that f(x)<3.

// 2. Check the rejection algorithm
function x=mybetarejectrnd(a,b,c)
    while (%t)
        y=distfun_unifrnd(0,1)
        u=distfun_unifrnd(0,1)
        f=distfun_betapdf(y,a,b)
        // g=distfun_unifpdf(y,0,1) = 1
        if (u<f/c) then
            x=y
            break
        end
    end
endfunction

a=2;
b=5;
c = 3; // An upper bound.
R=1000;
x=zeros(R);
for i=1:R
    x(i)=mybetarejectrnd(a,b,c);
end
scf();
limits=linspace(0,1,20);
histplot(limits,x);
x=linspace(0,1);
y=distfun_betapdf(x,a,b);
plot(x,y);
legend(["Data","Beta(a,b)"]);
xtitle("Rejection algorithm","X","Density")
