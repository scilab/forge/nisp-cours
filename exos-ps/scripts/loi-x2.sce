// Supposons que Xi ~ N(0,sigma^2).
// Soit Yn=sqrt(n) (Sn-sigma^2).
// où Sn = (1/n) (x1^2+...+xn^2)
// Alors 
// Yn -> N(0,2sigma^4) quand n->INF.
//
// Reference
// David R. Hunter. Statistics 553 : Asymptotic tools, 2011. 
// Penn. State University, 
// http://sites.stat.psu.edu/~dhunter/asymp/lectures/asymp.pdf

function Y=squaresrnd(R,n,sigma)
    // Generates R outcomes from the random variable Yn
    Y=zeros(R);
    for i=1:R
        x=distfun_normrnd(0,sigma,[1 n]);
        Sq=sum(x.^2);
        Y(i)=sqrt(n)*(Sq/n-sigma^2);    
    end
endfunction

function plotdistribution(R,n,sigma)
    Y=squaresrnd(R,n,sigma);
    histo(Y,[],[],1);
    x=linspace(-6*sigma^2,6*sigma^2);
    y=distfun_normpdf(x,0,sqrt(2)*sigma^2);
    plot(x,y)
    strtitle=msprintf("Asymptotic distribution, n=%d",n)
    xtitle(strtitle,"X","Density");
endfunction

R=1000;
sigma=3;
scf();
subplot(2,2,1)
plotdistribution(R,1,sigma)
subplot(2,2,2)
plotdistribution(R,10,sigma)
subplot(2,2,3)
plotdistribution(R,20,sigma)
subplot(2,2,4)
plotdistribution(R,50,sigma)
