// Example of rejection sampling. 
// Reference
// Karl Sigman. Acceptance-rejection method, 2007. 
// Professor Karl Sigman’s Lecture Notes on Monte Carlo Simulation, 
// Columbia University, 
// http://www.columbia.edu/~ks20/4703-Sigman/4703-07-Notes-ARM.pdf.

// Random Number Generation
// Rejection method
// http://dept.stat.lsa.umich.edu/~kshedden/Courses/Stat606/Notes/random.pdf
// Kerby Shedden
// Statistics 606: Computational Statistics

// See how to generate standard normal random variables, 
// using a rejection algorithm 
// and a instrumental Cauchy distribution.

// Simulate n standard Cauchy random variables
function x=mycauchyrnd(n)
    u=distfun_unifrnd(0,1,[n 1])
    x=tan(%pi*(u-0.5))
endfunction

// 1. Check the Cauchy generator
n=1000;
data=mycauchyrnd(n);
scf();
x=linspace(-5,5,20);
histplot(x,data);
x=linspace(-5,5);
y=distfun_tpdf(x,1);
plot(x,y)
xtitle("Cauchy distribution","X","Density")

// 2. Check the distributions
x=linspace(-3,3);
y=distfun_normpdf(x,0,1);
scf();
plot(x,y,"r-")
c = sqrt(2*%pi/%e);
y=distfun_tpdf(x,1);
plot(x,c*y,"b-")
legend(["Normal(0,1)","c*Cauchy"])
xtitle("Rejection algorithm","X","Density")

// 3. Check the rejection algorithm
function x=mynormrejectrnd()
    c = sqrt(2*%pi/%e);
    while (%t)
        y=mycauchyrnd(1)
        u=distfun_unifrnd(0,1)
        f=distfun_normpdf(y,0,1)
        g=distfun_tpdf(y,1)
        if (u<f/c/g) then
            x=y
            break
        end
    end
endfunction

R=1000;
for i=1:R
    z(i)=mynormrejectrnd();
end
scf();
x=linspace(-3,3,20);
histplot(x,z);
x=linspace(-3,3);
y=distfun_normpdf(x,0,1);
plot(x,y);
legend(["Data","Normal(0,1)"]);
xtitle("Rejection algorithm","X","Density")

// See why f(x)/g(x) <= sqrt(2*%pi/%e)
x=linspace(-5,5);
// Compute q(x)=f(x)/g(x)
q=sqrt(%pi/2)*(1+x.^2).*exp(-0.5*x.^2);
scf();
plot(x,q);
xtitle("Ratio f/g","x","f(x)/g(x)");
