// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Build a LHS design with 2 variables


function sampling = nisp_buildlhs ( s , n )
  // Creates a LHS design
  //
  // Calling Sequence
  //   sampling = nisp_buildlhs ( s , n )
  //
  // Parameters
  // s: a 1-by-1 matrix of floating point integers, the number of variables
  // n: a 1-by-1 matrix of floating point integers, the number of points
  // sampling: a n by-s matrix of doubles, where sampling(i,j) in [0,1] is the value of the parameter #j for the experiment #i. 
  //
  // Description
  //   This function builds a classical LHS design.
  //   This function changes the state of the grand uniform random number generator.
  //
  // Examples
  // // Create a Lhs design with 5 points in 2 dimensions.
  //   n = 5;
  //   s = 2;
  //   sampling = lhsampling ( s , n );
  //   scf();
  //   plot ( sampling(:,1) , sampling(:,2) , "bo" );
  //   // Add the cuts
  //   cut = linspace ( 0 , 1 , n + 1 );
  //   for i = 1 : n + 1
  //     plot( [cut(i) cut(i)] , [0 1] , "-" )
  //   end
  //   for i = 1 : n + 1
  //     plot( [0 1] , [cut(i) cut(i)] , "-" )
  //   end
  //
  // // Create a Lhs design with 1000 points in 2 dimensions.
  //   n = 1000;
  //   s =  2;
  //   scf();
  //   sampling = lhsampling ( s , n );
  //   plot ( sampling(:,1) , sampling(:,2) , "bo" );
  //   scf();
  //   histplot ( 50 , sampling(:,1));
  //   scf();
  //   histplot ( 50 , sampling(:,2));
  //
  // // Create a Lhs design with 1000 points in 2 dimensions.
  // // The first variable is normal, the second is uniform.
  // n = 1000;
  // s =  2;
  // scf();
  // sampling = lhsampling ( s , n );
  // // First variable is normal
  // mu = 2;
  // sigma=3;
  // P = sampling(:,1);
  // Q = 1-sampling(:,1);
  // sampling(:,1)=cdfnor("X",mu*ones(n,1),sigma*ones(n,1),P,Q);
  // // Second variable is uniform : do nothing
  // plot ( sampling(:,1) , sampling(:,2) , "bo" );
  // scf();
  // histplot ( 50 , sampling(:,1));
  // scf();
  // histplot ( 50 , sampling(:,2));
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // "A User's Guide to LHS: Sandia's Latin Hypercube Sampling Software", Gregory D. Wyss and Kelly H. Jorgensen, 1998
  // "A Comparison of Three Methods for Selecting Values of Input Variables in the Analysis of Output from a Computer Code", M. D. McKay, R. J. Beckman, W. J. Conover, Technometrics, Vol. 21, No. 2 (May, 1979), pp. 239-245

  cut = linspace ( 0 , 1 , n + 1 )'
  // Fill points uniformly in each interval
  u = grand(n,s,"unf",0,1)
  a = cut(1 : n)
  b = cut(2 : n+1)
  rdpoints = zeros(n,s)
  for j = 1 : s
    rdpoints(:,j)  = u(:,j) .* (b-a) + a
  end
  // Make the random pairings
  sampling = zeros(n,s)
  for j = 1 : s
    order = grand(1,"prm",(1:n)')
    sampling ( 1 : n , j ) = rdpoints ( order , j )
  end
endfunction


