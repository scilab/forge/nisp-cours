// Copyright (C) 2010 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Build a LHS design with 2 variables

// References
//
// "A User's Guide to LHS: Sandia's Latin Hypercube Sampling Software"
// Gregory D. Wyss and Kelly H. Jorgensen
// 1998
//
// "A Comparison of Three Methods for Selecting Values of Input Variables in the Analysis of
// Output from a Computer Code"
// M. D. McKay, R. J. Beckman, W. J. Conover
// Technometrics, Vol. 21, No. 2 (May, 1979), pp. 239-245

// n: the number of points
// s: the number of variables
// sampling: a n x s matrix, where each row represents the experiment
function sampling = lhsampling ( s , n )
  cut = linspace ( 0 , 1 , n + 1 )'
  // Fill points uniformly in each interval
  u = grand(n,s,"unf",0,1)
  a = cut(1 : n)
  b = cut(2 : n+1)
  rdpoints = zeros(n,s)
  for j = 1 : s
    rdpoints(:,j)  = u(:,j) .* (b-a) + a
  end
  // Make the random pairings
  sampling = zeros(n,s)
  for j = 1 : s
    order = grand(1,"prm",(1:n)')
    sampling ( 1 : n , j ) = rdpoints ( order , j )
  end
endfunction

scf();
n = 5;
s =  2;
sampling = lhsampling ( s , n );
plot ( sampling(:,1) , sampling(:,2) , "bo" );
// Add the cuts
cut = linspace ( 0 , 1 , n + 1 );
for i = 1 : n + 1
  plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : n + 1
  plot( [0 1] , [cut(i) cut(i)] , "-" )
end

scf();
n = 1000;
s =  2;
sampling = lhsampling ( s , n );
plot ( sampling(:,1) , sampling(:,2) , "bo" );

scf();
histplot ( 50 , sampling(:,1));

scf();
histplot ( 50 , sampling(:,2));

