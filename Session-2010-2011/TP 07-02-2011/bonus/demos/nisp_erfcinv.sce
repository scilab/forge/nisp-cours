//
// This help file was automatically generated from nisp_erfcinv.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_erfcinv.sci
//

nisp_erfcinv(1.e-3)
expected = 2.326753765513525
halt()   // Press return to continue
 
// Plot the function
scf();
x = linspace(1.e-5,2-1.e-5,1000);
y = nisp_erfcinv(x);
plot(x,y);
halt()   // Press return to continue
 
// This implementation is accurate, even if x is small:
nisp_erfcinv ( 10^-20 )
expected = 6.6015806223551425615163916324187
// By contrast, the mathematically correct erfinv(1-x)
// formula gives poor results:
x = 10^-20
erfinv(1-x)
expected = 6.6015806223551425615163916324187
halt()   // Press return to continue
 
// erfcinv is ill-conditioned when x is close to 1.
nisp_erfcinv(1-1.e-15)
expected = 8.862269254527580136e-16
halt()   // Press return to continue
 
// erfcinv is ill-conditioned when x is close to 2.
nisp_erfcinv(2-1.e-12)
expected = -5.04202974563905937
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_erfcinv.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
