// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Test the chaos polynomial decomposition on the Ishigami function.
// Three uniform variables in [-pi,pi].
// Create a Petras sampling and compute the coefficients of the polynomial 
// by integration.
//

function y = ishigami (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction

// 3 random variable uniform in [-pi,pi]
a=7.;
b=0.1;
// Expectation
exact.expectation = a/2;
// Expectation
exact.var = 1/2 + a^2/8 + b*%pi^4/5 + b^2*%pi^8/18;

// 1. Create a group of stochastic variables
nx = 3;
srvx = setrandvar_new( nx );

// 2. Create a group of uncertain variables
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 3. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
setrandvar_buildsample( srvu , srvx );

// 4. Create the chaos polynomial
pc = polychaos_new ( srvx , 1 );
polychaos_setdegree(pc,degre);

// 5. Perform the experiments
np = setrandvar_getsize(srvu);
mprintf("Number of experiments = %d\n",np);
polychaos_setsizetarget(pc,np);
inputdata = setrandvar_getsample(srvu);
outputdata = ishigami(inputdata);
polychaos_settarget(pc,outputdata);

// 6. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,"Integration");

// 7. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);

mprintf("Mean        = %f (expected=%f)\n",average,exact.expectation);
mprintf("Variance    = %f (expected=%f)\n",var,exact.var);
disp (" First order sensitivity indice : "); 
disp ( polychaos_getindexfirst (pc )')
disp (" Total sensitivity indices: "); 
disp ( polychaos_getindextotal (pc )')

// Calcul de la part d'incertitude expliquée par
// le groupe des variables X1 et X2
groupe = [1 3];
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , groupe(1) );
polychaos_setgroupaddvar ( pc , groupe(2) );
mprintf("Fraction of the variance of a group of variables\n");
mprintf("    Groupe X1 et X2 =%f\n",polychaos_getgroupind(pc));

// Edition de la décomposition fonctionnelle de la variance
// la ligne 1 0 1 indique le groupe de variables X1 et X3
// la valeur qui suit, indiquera la part de la variance expliquée par ce groupe
// La somme de tous les termes est égale à 1 (variance normalisée)
mprintf("Anova decomposition of the normalized variance.\n");
polychaos_getanova(pc);

// Tracer de l'histogramme illustrant la densité de probabilité de la réponse
// approchée par le polynôme de chaos
polychaos_buildsample(pc,"Lhs",10000,0);
sample_output = polychaos_getsample(pc);
my_handle = scf(10001);
histplot(50,sample_output)
xtitle("Ishigami - Histogram");

// Tracer des indices de sensibilité
for i=1:nx
  indexfirst(i)=polychaos_getindexfirst(pc,i);
  indextotal(i)=polychaos_getindextotal(pc,i);
end
my_handle = scf(10002);
bar(indextotal,0.2,'blue');
bar(indexfirst,0.15,'yellow');
legend(["Total" "First order"],pos=1);
xtitle("Ishigami - Sensitivity index");
//
// Clean-up
//
randvar_destroy ( rvu1 );
randvar_destroy ( rvu2 );
randvar_destroy ( rvu3 );
setrandvar_destroy ( srvu );
polychaos_destroy ( pc );
setrandvar_destroy ( srvx );
//
// Load this script into the editor
//
filename = 'polychaos_ishigami.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

