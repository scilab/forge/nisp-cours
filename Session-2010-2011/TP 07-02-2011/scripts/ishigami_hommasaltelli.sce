// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices of the ishigami function.
// Uses the Sobol, Homma, Saltelli method.
// Three random variables uniform in [-pi,pi].
// Uses Monte-Carlo experiments to compute the sensitivity indices.
//

tic();

function y = ishigami (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction

function C = nisp_cov ( x , y )
    // Returns the empirical covariance matrix of x and y.
  x=x(:)
  y=y(:)
  n = size(x,"*")
  x=x-mean(x)
  y=y-mean(y)
  C(1,1) = x'*x/(n-1)
  C(1,2) = x'*y/(n-1)
  C(2,1) = C(1,2)
  C(2,2) = y'*y/(n-1)
endfunction

function s = sensitivityindex(ya,yc)
  // Returns the sensitivity index associated with experiments ya and yc.
  C = nisp_cov (ya, yc)
  s= C(1,2)/ (st_deviation(ya) * st_deviation(yc))
endfunction

// Create the uncertain parameters
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
// The number of uncertain parameters is :
nx = setrandvar_getdimension(srvu);
np = 10000;
// Create a first sampling A
setrandvar_buildsample(srvu,"Lhs",np);
A = setrandvar_getsample(srvu);
// Create a first sampling B
setrandvar_buildsample(srvu,"Lhs",np);
B = setrandvar_getsample(srvu);
// Perform the experiments in A
ya = ishigami(A);
// Compute the first order sensitivity index for X1
C = B;
C(1:np,1)=A(1:np,1);
yc = ishigami(C);
s1 = sensitivityindex(ya,yc);
mprintf("S1 : %f (expected = %f)\n", s1, 0.3139);
// Compute the first order sensitivity index for X2
C = B;
C(1:np,2)=A(1:np,2);
yc = ishigami(C);
s2 = sensitivityindex(ya,yc);
mprintf("S2 : %f (expected = %f)\n", s2, 0.4424);
// Compute the first order sensitivity index for X3
C = B;
C(1:np,3)=A(1:np,3);
yc = ishigami(C);
s3 = sensitivityindex(ya,yc);
mprintf("S3 : %f (expected = %f)\n", s3, 0.0);
// Compute the first order sensitivity index for {X1,X3}
C = A;
C(1:np,2)=B(1:np,2);
yc = ishigami(C);
s13 = sensitivityindex(ya,yc);
mprintf("S13 : %f (expected = %f)\n", s13, 0.5576);

//
// Clean-up
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
setrandvar_destroy(srvu);

t = toc();
mprintf("Time = %f (s)\n", t);

