// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices, the naive way.
// Requires d*np^2 function evaluations, where np is the number of 
// experiments and d is the number of variables.
// Here np = 1000 and d = 2.
//

tic();

function y = Exemple (x)
  y(:,1) = x(:,1) .* x(:,2)
endfunction

// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;

// 
// We apply the definition of the first order sensitivity 
// indice of the first variable is 
// S1 = V(E(y|x1))/V(y)
//
// 1. Compute the variable of y.
//
// Begin with np = 10.
// Serious computation is with np >= 1000.
// More than this cannot be done in practice with this method.
np = 1000;
mprintf("Number of experiments : %d\n",1+2*np^2);

// Define a sampling for x1.
u = grand(np,1,"def");
x1 = cdfnor("X",mu1*ones(np,1),sigma1*ones(np,1),u,1-u);
// Define a sampling for x2.
u = grand(np,1,"def");
x2 = cdfnor("X",mu2*ones(np,1),sigma2*ones(np,1),u,1-u);
// Merge the two samplings
x = [x1 x2];
// Perform the experiments
y = Exemple (x);
Ey= mean(y);
Ey_expectation= mu1*mu2;
Vy= variance(y);
Vy_expectation = mu2^2*sigma1^2 + mu1^2*sigma2^2 + sigma1^2*sigma2^2;
mprintf("Expectation y = %.5f (expectation = %.5f)\n", Ey, Ey_expectation);
mprintf("Variance y = %.5f (expectation = %.5f)\n", Vy, Vy_expectation);
//
// 2. Compute E(y|x1) for several values of x1.
// Define a sampling for x1.
u = grand(np,1,"def");
x1 = cdfnor("X",mu1*ones(np,1),sigma1*ones(np,1),u,1-u);
//
for i = 1 : np
  // For each value of x1, define a sampling for x2.
  u = grand(np,1,"def");
  x2 = cdfnor("X",mu2*ones(np,1),sigma2*ones(np,1),u,1-u);
  // Merge the current value of x1, i.e. x1(i), 
  // with the sampling for x2.
  x = [x1(i)*ones(np,1) x2];
  // Perform the experiments
  y = Exemple (x);
  // Store E(y|x1) in Ey_all(i)
  Ey_all(i) = mean(y);
end
//
// Compute S1
S1 = variance(Ey_all)/Vy;
S1_expectation = ( mu2^2*sigma1^2 ) / Vy_expectation;
mprintf("First order sensitivity / x1 = %.5f (expectation = %.5f)\n", S1, S1_expectation);
re = abs(S1- S1_expectation)/S1_expectation;
mprintf("  Relative Error = %f\n", re);
//
// 3. Compute E(y|x2) for several values of x2.
// Define a sampling for x2.
u = grand(np,1,"def");
x2 = cdfnor("X",mu2*ones(np,1),sigma2*ones(np,1),u,1-u);
//
for i = 1 : np
  // For each value of x2, define a sampling for x1.
  u = grand(np,1,"def");
  x1 = cdfnor("X",mu1*ones(np,1),sigma1*ones(np,1),u,1-u);
  // Merge the current value of x2, i.e. x2(i), 
  // with the sampling for x1.
  x = [x1 x2(i)*ones(np,1)];
  // Perform the experiments
  y = Exemple (x);
  // Store E(y|x2) in Ey_all(i)
  Ey_all(i) = mean(y);
end
//
// Compute S1
S2 = variance(Ey_all)/Vy;
S2_expectation = ( mu1^2*sigma2^2 ) / Vy_expectation;
mprintf("First order sensitivity / x2 = %.5f (expectation = %.5f)\n", S2, S2_expectation);
re = abs(S2- S2_expectation)/S2_expectation;
mprintf("  Relative Error = %f\n", re);

t = toc();
mprintf("Time = %f (s)\n", t);

