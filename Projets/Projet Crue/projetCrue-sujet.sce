// Copyright (C) 2013 - 2014 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Projet Crue
// Sujet

function S = surverse(x)
    Q=x(:,1) // Debit (m3/s)
    Ks=x(:,2) // Coeff. Mannning-Strickler (m^(1/3)/s)
    Zv=x(:,3) // Cote du fond de la riviere en aval (m)
    Zm=x(:,4) // Cote du fond de la riviere en amont (m)
    Hd=x(:,5) // Hauteur de la digue (m)
    Zb=x(:,6) // Cote de la berge (m)
    L=x(:,7) // Longueur du troncon de riviere (m)
    B=x(:,8) // Largeur de la riviere (m)
    //
    pente=(Zm-Zv)./L // Si la pente est petite
    H=(Q./(Ks.*B.*sqrt(pente))).^(3/5)
    S=Zv+H-Hd-Zb
endfunction

function r = wilks(alpha,bet,n)
    // Calcule le rang r, tel que :
    // P(Y(r)>y)>bet
    // ou y le quantile de probabilite alpha, i.e.
    // P(Y<y)=alpha.
    // Si il n'y a pas assez de donnees, renvoit r=0.
    // Require: distfun
    if (n < log(1-bet)/log(alpha)) then 
        r=0;
    else
        r = distfun_binoinv(bet,n,alpha)
        r = r + 1
    end
endfunction

/////////////////////////////////////////////////////
//
// Parametres des distributions
//
Ksmu=30;
Kssigma=7.5;
//
Qmu=1013;
Qsigma=558;
//
Zma=54;
Zmb=56;
//
Zva=49;
Zvb=51;
//
// Parametres deterministes
Hd=3;
Zb=55.5;
L=5000;
B=300;

/////////////////////////////////////////////////////
//
// Verification du modele
//
// Debit maximal historique
x=[];
x(1)=3854; // Debit (m3/s)
x(2)=15; // Coeff. Mannning-Strickler (m^(1/3)/s)
x(3)=51; // Cote du fond de la riviere en aval (m)
x(4)=54; // Cote du fond de la riviere en amont (m)
x(5)=0; // Hauteur de la digue (m)
x(6)=55.5; // Cote de la berge (m)
x(7)=5000; // Longueur du troncon de riviere (m)
x(8)=300; // Largeur de la riviere (m)
x=x';
S = surverse(x)
S+Zb
// Valeur attendue : 59.4 (m)

/////////////////////////////////////////////////////
//
// Graphique de la PDF
//

TODO

/////////////////////////////////////////////////////
//
// Monte-Carlo
//
N=100000;
Ks=TODO
Q=TODO
Zm=TODO
Zv=TODO
// Troncature:
i=find(Q<0|Ks<0);
Ks(i)=[];
Q(i)=[];
Zm(i)=[];
Zv(i)=[];
N=size(Ks,"r");
mprintf("Nombre de simulations=%d\n" , N);
//
// Histogrammes des entrees
//
TODO
//
x=zeros(N,8);
x(:,1)=Q; // Debit (m3/s)
x(:,2)=Ks; // Coeff. Mannning-Strickler (m^(1/3)/s)
x(:,3)=Zv; // Cote du fond de la riviere en aval (m)
x(:,4)=Zm; // Cote du fond de la riviere en amont (m)
x(:,5)=Hd; // Hauteur de la digue (m)
x(:,6)=Zb; // Cote de la berge (m)
x(:,7)=L; // Longueur du troncon de riviere (m)
x(:,8)=B; // Largeur de la riviere (m)
S = surverse(x);
//
// Scatter plot
TODO

// Histogramme de la surverse
TODO

/////////////////////////////////////////////////////
//
// Calcul de la probabilite de defaillance
//

// Estime Pf
nfail = TODO
Pf = TODO
mprintf("Nombre de crues=%d\n" , nfail);
mprintf("Pf = %e\n" , Pf);

// Calcul d'un intervalle de confiance pour Pf
level=1.-0.95;
low = TODO
up = TODO
mprintf("95%% Int. de Conf.:[%e,%e]\n" , low,up);

/////////////////////////////////////////////////////
//
// Estimation des quantiles
//
TODO
mprintf("Surverse moyenne:%f (m)\n",m);

//
// Quantile a 1-0.1 (crue decenale)
TODO
mprintf("Quantile a 1-0.1:%f (m)\n",S(i));
// Quantile a 1-0.01 (crue centennale)
TODO
mprintf("Quantile a 1-0.01:%f (m)\n",S(i));
// Quantile a 1-0.001 (crue millenale)
TODO
mprintf("Quantile a 1-0.001:%f (m)\n",S(i));

// Quantile de Wilks avec confiance de 95%
TODO
mprintf("Quantile a 1-0.001 (95%% de confiance):%f (m)\n",S(i));
// Quantile de Wilks avec confiance de 95%
TODO
mprintf("Quantile a 1-0.1 (95%% de confiance):%f (m)\n",S(i));
// Quantile de Wilks avec confiance de 95%
TODO
mprintf("Quantile a 1-0.01 (95%% de confiance):%f (m)\n",S(i));
