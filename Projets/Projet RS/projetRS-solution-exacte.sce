// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet R-S
// Solution

// Reference
// F. Deheeger
// "Couplage mecano-fiabiliste : 2SMART, 
// methodologie d'apprentissage stochastique en fiabilite."
// These, 2008
// "IV.4.1 Validation de l'apprentissage
// la methode SMART, Le cas R-S"
// p 118

// Variable R - Normale(7,1)
muR=7;
sigmaR=1;
// Variable S - Normale(2,1)
muS=2;
sigmaS=1;

//
// 2. Exact computation

// 2.a Exact computation in the general case
function y=myf(s,muR,sigmaR,muS,sigmaS)
    FRs = distfun_normcdf(s,muR,sigmaR)
    fSs = distfun_normpdf(s,muS,sigmaS)
    y = FRs*fSs
endfunction
[PfExact,err]=intg(0,10,list(myf,muR,sigmaR,muS,sigmaS));
mprintf("Exact Pf=%e\n" ,PfExact);
mprintf("Error=%e\n" ,err);
