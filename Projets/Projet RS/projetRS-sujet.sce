// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet R-S
// Sujet

// Analytical model definition:
function G = LimitState ( x )
    R = x(:,1)
    S = x(:,2)
    G = R-S
endfunction

// 1. Test of the limit state function:
x = [
7 2
-6 1
8 3
];
strx=string(x(:,1))+","+string(x(:,2))
mprintf ("G(%s)=%f\n" , strx, LimitState(x) );

// Variable R - Normale(7,1)
muR=7;
sigmaR=1;
// Variable S - Normale(2,1)
muS=2;
sigmaS=1;

//
// 2. Exact computation

// 2.a Exact computation in the general case
function y=myf(s,muR,sigmaR,muS,sigmaS)
    y = TODO
endfunction
[PfExact,err]=intg(TODO);
mprintf("Exact Pf=%e\n" ,PfExact);
mprintf("Error=%e\n" ,err);

// 2.b Exact computation, when R and S are normal
mu=TODO
sigma=TODO
pfexact=TODO

// 3. Generate a Simple Random Sampling
NbSim=100000;
// Variable R
R=TODO
// Variable S
S=TODO

// 4. Histograms of R and S
TODO
xtitle("PDF of R","R","Frequency")
TODO
xtitle("PDF of S","S","Frequency")

// 5. Put the two histograms on the same plot
TODO

// 6. Monte-Carlo
x=[R,S];
TODO

// 7. Histogram of R-S
TODO
xtitle("Test R-S","R-S","Frequency")

// 8. Estimate Pf
TODO
nfail = TODO
Pf = TODO
mprintf("Number of failures=%d\n" , nfail);
mprintf("Pf = %e\n" , Pf);

// 9. Compute a confidence interval of Pf
TODO
low = TODO
up = TODO
mprintf("95%% Conf. Int.:[%e,%e]\n" , low,up);

// 10. Creer une figure des simulations reussies/defaillances
// See "Fig. IV.2 [Cas R - S]", [1], p124
TODO
xtitle("Test R-S","R","S");
legend(["Success","Failures"]);

// 11. Convergence des estimateurs
imax=23;
stacksize("max");
nbsimarray=[];
relerr=[];
mprintf("NbSim, nfail, Pf:\n" );
for i=1:imax
    NbSim=2^i;
    TODO
    nbsimarray(i)=NbSim;
    relerr(i)=abs(PfExact-Pf)/PfExact;
end
TODO
xtitle("Test R-S","Number of samples","Relative error");

//
// 12. Polynomes de chaos sur R-S
//

// Estimation de la probabilite de defaillance

// 1. Une collection de nx variables stochastiques
mprintf("Developpement en Polynomes de Chaos\n")
srvx = setrandvar_new();
rvxR = randvar_new("Normale");
setrandvar_addrandvar ( srvx, rvxR);
rvxS = randvar_new("Normale");
setrandvar_addrandvar ( srvx, rvxS);
// 2. Une collection de nx variables incertaines
srvu = setrandvar_new();
srvuR = randvar_new("Normale",muR,sigmaR);
setrandvar_addrandvar ( srvu, srvuR);
srvuS = randvar_new("Normale",muS,sigmaS);
setrandvar_addrandvar ( srvu, srvuS);
// 3. Le plan d'experiences
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Cree le polynome de chaos
TODO
// 5. Realise le plan d'experiences
inputdata = TODO
mprintf("Number of simulations: %d\n",size(inputdata,"r"))
// 6. Calcule les coefficients du P.C.
TODO
// 7. Calcule un echantillon interne, par Monte-Carlo
TODO
// 8. Estime P(G<0)
p = TODO
mprintf("p=%e (exact=%e)\n",p,PfExact);
// Clean-up
nisp_destroyall();
