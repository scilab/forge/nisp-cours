// Modele mathematique a analyser : fonction produit
function y = MyFunction (x , alpha)
  y=1;
  for i=1:size(x,2)
    // %eps est la precison machine
    if (abs(x(i)) < %eps) then 
      a = 2 * i + 1;
    else
      a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
    end
    b = (a - 1) / sqrt(2 * i);
    y = y * (1 + alpha(i) * b);
  end
endfunction

// Dimension 
d=3;
for i=1:d
  alpha(i) = 1. / i;;
end

// Calcul de la moyenne de Y
muy = 1;

// Calcul de la variance de Y
pr  = 1.;
for i=1:d
  pr = pr * (1 + alpha(i)^2);
end
vay = pr - 1;

// Calcul des indices du premier ordre et des indices totaux
sx=ones(d,1);
for i=1:d
  sx(i) = (alpha(i)^2) / vay;
end
st=ones(d,1);
for i=1:d
  a     = pr/(1 + alpha(i)^2);
  st(i) = ((1 + vay) / vay) * (alpha(i)^2 / (1 + alpha(i)^2));
end


nx=d;
srvx = setrandvar_new();
for i=1:d
  rvx(i) = randvar_new("Uniforme");
  setrandvar_addrandvar(srvx, rvx(i));
end

// On verifie en editant sur la console les parametres des variables X(i)
// setrandvar_getlog(srvx);

// Specification d'un plan : quadrature tensorisee
// formule excate pour un polyn�me de degre = degre
degre = 10;
setrandvar_buildsample( srvx, "Lhs", 2000);


// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );

// Realisation du plan d'experiences numeriques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata  = zeros(nx);
outputdata = zeros(ny);
for k=1:np
  inputdata  = setrandvar_getsample(srvx,k);
  outputdata = MyFunction(inputdata, alpha);
  polychaos_settarget(pc,k,outputdata);
end

// Calcul des coefficients par integration numerique
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Regression");

// Edition de l'analyse de sensibilite
mprintf("Nombre de simulations %d\n",setrandvar_getsize(srvx));
mprintf("Mean        = %f (expected %f) \n",polychaos_getmean(pc),muy);
mprintf("Variance    = %f (expected %f) \n",polychaos_getvariance(pc),vay);
mprintf("Indice de sensibilite du 1er ordre\n");
for i=1:d
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindexfirst(pc,i),sx(i));
end
mprintf("Indice de sensibilite Totale\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindextotal(pc,i),st(i));
end


