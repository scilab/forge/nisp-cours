//
// Master M2S - Projet
// script Sobol2.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique a analyser
// fonctions noyaux de Dirichlet (voir bench JMM Mascot Num)
function y = MyFunction (x)
    y=1;
    for i=1:size(x,2)
        // %eps est la precision machine
        if (abs(x(i)) < %eps) then 
            a = 2 * i + 1;
        else
            a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
        end
        b = (a - 1) / sqrt(2 * i);
        y = y * (1 + alpha(i) * b);
    end
endfunction


// Dimension 
d=3;
// alpha = [1/2 1/3 1/4]; // examen
alpha = [1/3 1/4 1/5];    // sujet

// Calcul de la moyenne de Y
muy = 1;
// Calcul de la variance de Y
cv2 = alpha .^2;
vay = muy^2 * (prod(cv2 + 1) - 1);

sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
    sx(i) = cv2(i) / (prod(cv2+1) - 1);
    sg(i) = sx(i)  * prod(cv2+1) / (cv2(i)+1);
end


function [s1,s1min,s1max,st,stmin,stmax] = sobol(A,B)
    [n,d] = size(A);
    s1    = zeros(d,1);
    s1min = zeros(d,1);
    s1max = zeros(d,1);
    st    = zeros(d,1);
    stmin = zeros(d,1);
    stmax = zeros(d,1);
    ya    = zeros(n,1);
    yb    = zeros(n,1);
    yc    = zeros(n,1);
    for k=1:n
        ya(k) = MyFunction(A(k,:));
        yb(k) = MyFunction(B(k,:));
    end
    niveau = 1.96/sqrt(n-3.);
    for i=1:d
        C = B;
        C(:,i) = A(:,i);
        for k=1:n
            yc(k) = MyFunction(C(k,:));
        end

        // Calcul des intervalles de confiance
        rho   = corrcoef(ya,yc);
        z     = 0.5 * log((1+rho)/(1-rho));
        s1(i)    = min(max(0., rho),1.);
        s1min(i) = max(0., tanh(z - niveau));
        s1max(i) = max(0., tanh(z + niveau));

        rho   = corrcoef(yb,yc);
        z     = 0.5 * log((1+rho)/(1-rho));
        st(i)    = min(max(0., 1 - rho),1.);
        stmax(i) = max(0., 1. - tanh(z - niveau));
        stmin(i) = max(0., 1. - tanh(z + niveau));
    end
endfunction

// On regroupe les variables
srvu = setrandvar_new();
for i=1:d
    rvu(i) = randvar_new("Uniforme",0,1);
    setrandvar_addrandvar(srvu, rvu(i));
end


kmax = 20;
S1    = zeros(d,kmax);
S1min = zeros(d,kmax);
S1max = zeros(d,kmax);
St    = zeros(d,kmax);
Stmin = zeros(d,kmax);
Stmax = zeros(d,kmax);
Iter  = zeros(1,kmax);

for k=1:kmax
    mprintf("k = %d\n",k);
    n = 100 * k;
    Iter(k) = n*(d+2);
    setrandvar_buildsample(srvu,"MonteCarlo",n);
    A = setrandvar_getsample(srvu);
    setrandvar_buildsample(srvu,"MonteCarlo",n);
    B = setrandvar_getsample(srvu);
    [s1,s1min,s1max,st,stmin,stmax] = sobol(A,B);
    S1(:,k)    = s1;
    S1min(:,k) = s1min;
    S1max(:,k) = s1max;
    St(:,k)    = st;
    Stmin(:,k) = stmin;
    Stmax(:,k) = stmax;
end

clf;
j = 0;
for r=1:d
    //
    // Indice du premier ordre
    j=j+1;
    subplot(d,2,j);
    plot(Iter,S1(r,:),'r-');
    plot(Iter,S1min(r,:),'g-.');
    plot(Iter,S1max(r,:),'g-.');
    true = zeros(1,kmax);
    for k=1:kmax
        true(k) = sx(r);
    end
    plot(Iter,true,'b');
    xlabel("Nombre de simulations")
    ylabel("S"+string(r))
    chaine = 'Indice du premier ordre - Variable ' + string(r);
    xtitle(chaine);
    //
    // Indice Total
    j = j + 1;
    subplot(d,2,j);
    plot(Iter,St(r,:),'r-');
    plot(Iter,Stmin(r,:),'g-.');
    plot(Iter,Stmax(r,:),'g-.');
    true = zeros(1,kmax);
    for k=1:kmax
        true(k) = sg(r);
    end
    plot(Iter,true,'b');
    xlabel("Nombre de simulations")
    ylabel("ST"+string(r))
    chaine = 'Indice de sensibilite totale - Variable ' + string(r);
    xtitle(chaine);
end
