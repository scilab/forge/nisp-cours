//////////////////////////////////////////////////////////////////////////////////////////
// Analyse de sensibilite
// Methode de Sobol + intervalles de confiance
//
// A et B les 2 matrices de meme taille regroupant les 2 plans d'experiences
// MyFunction la fonction codant le modele mathematique ou l'appel au code de calcul
//
// s1 estimation des indices du premier ordre et les bornes [s1min,s1max] a 95%
// st estimations des indices de sensibilie totale et les bornes [stmin,stmax] a 95%
// Remarque : les estimations sont calculees sans tenir compte de la positivite des indices
// de sensibilite. Par contre pour calculer les intervalles de confiance il a ete tenu
// copmpte des contraintes sur les indices (valeurs comprises entre 0 et 1)
//
// Remarque : ce qui n'est pas encore fait : je n'ai pas su prendre en compte le fait
// que l'indice de sensibilite globale est plus grande que celle du premier ordre
///////////////////////////////////////////////////////////////////////////////////////////
function [s1, s1min, s1max, st, stmin, stmax] = sobol(A, B, MyCode)
  [n,d] = size(A);
  s1    = zeros(d, 1);
  s1min = zeros(d, 1);
  s1max = zeros(d, 1);
  st    = zeros(d, 1);
  stmin = zeros(d, 1);
  stmax = zeros(d, 1);
  ya    = zeros(n, 1);
  yb    = zeros(n, 1);
  yc    = zeros(n, 1);
  for k=1:n
    ya(k) = MyCode(A(k,:));
    yb(k) = MyCode(B(k,:));
  end
  niveau = 1.96/sqrt(n-3.);
  for i=1:d
    C = B;
    C(:,i) = A(:,i);
    for k=1:n
      yc(k) = MyCode(C(k,:));
    end
    // Estimations des indices du 1er ordre
    rho = nisp_corrcoef( ya , yc );
    if rho >= 1 then 
      s(i)     = 1;
      s1min(i) = 1;
      s1max(i) = 1;
    else
      rho      = max(0, rho);
      s(i)     = rho;
      z        = 0.5 * log( (1 + rho) / (1 - rho) );
      s1min(i) = max(0., tanh(z - niveau));
      s1max(i) = max(0., tanh(z + niveau));
    end

   // Estimation des indices de sensibilite totale ...
    rho      = nisp_corrcoef( yb , yc );
    if rho >= 1 then
      st(i)    = 0.;
      stmin(i) = 0.;
      stmax(i) = 0.;
    else
      rho      = max(0, rho);
      st(i)    = 1 - rho;
      z        = 0.5 * log( (1 + rho) / (1 - rho) );
      stmin(i) = max(0., 1. - tanh(z - niveau));
      stmax(i) = max(0., 1. - tanh(z + niveau));
    end
  end
endfunction
