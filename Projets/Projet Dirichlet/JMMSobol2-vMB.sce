//
// Master M2S - Projet
// script Sobol2.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique a analyser
// fonctions noyaux de Dirichlet (voir bench JMM Mascot Num)
function y = MyFunction (x)
    m=size(x,"r")
    n=size(x,"c")
    y=ones(m,1)
    for i=1:n
        a=zeros(m,1)
        j=find(x(:,i)==0)
        a(j)=2 * i + 1
        j=find(x(:,i)<>0)
        a(j) = sin((2*i+1) * %pi * x(j,i))./sin(%pi * x(j,i))
        b = (a - 1) / sqrt(2 * i)
        y = y .* (1 + alpha(i) * b)
    end
endfunction


// Dimension 
d=3;
alpha = [1/3 1/4 1/5];    // sujet

// Calcul de la moyenne de Y
muy = 1;
// Calcul de la variance de Y
cv2 = alpha .^2;
vay = muy^2 * (prod(cv2 + 1) - 1);

sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
    sx(i) = cv2(i) / (prod(cv2+1) - 1);
    sg(i) = sx(i)  * prod(cv2+1) / (cv2(i)+1);
end

// Génère m réalisations de la variable aléatoire X(i).
function x=myrandgen(m, i)
    x = distfun_unifrnd(0,1,m,1)
endfunction

stacksize("max");
tmax=1.; // Temps maximum avant arrêt
n=10; // Nombre d'appels initial
k=1; // Indice de ligne initial
maxrepeat=5; // Nombre maximum de répétitions Monte-Carlo
multfactor=1.5; // Multiplication factor
S1    = [];
St    = [];
Iter  = [];
runthis=%t;
while(runthis)
    for i=1:maxrepeat
        tic();
        [s,nbevalf]=nisp_sobolsaFirst(MyFunction,d,myrandgen,n);
        [st,nbevalf]=nisp_sobolsaTotal(MyFunction,d,myrandgen,n);
        t=toc();
        mprintf("n = %d, i=%d (t=%.2f)\n",n,i,t);
        Iter(k) = nbevalf;
        S1(:,k) = s;
        St(:,k) = st;
        if t>tmax then
            runthis=%f;
            break
        end
        k=k+1;
    end
    n = floor(multfactor*n);
end

scf();
j = 0;
for r=1:d
    //
    // Indice du premier ordre
    j=j+1;
    subplot(d,2,j);
    plot(Iter,abs(S1(r,:)-sx(r)),'ro');
    plot(Iter,1 ./sqrt(Iter),'b-');
    xlabel("Nombre de simulations")
    ylabel("Absolute Error on S"+string(r))
    chaine = 'Indice du premier ordre - Variable ' + string(r);
    xtitle(chaine);
    legend(["1/sqrt(n)","Monte-Carlo"]);
    g=gca();
    g.log_flags="lln";
    //
    // Indice Total
    j=j+1;
    subplot(d,2,j);
    plot(Iter,abs(St(r,:)-sg(r)),'ro');
    plot(Iter,1 ./sqrt(Iter),'b-');
    xlabel("Nombre de simulations")
    ylabel("Absolute Error on ST"+string(r))
    chaine = 'Indice de sensibilite totale - Variable ' + string(r);
    xtitle(chaine);
    legend(["1/sqrt(n)","Monte-Carlo"]);
    g=gca();
    g.log_flags="lln";
end
