//
// Master M2S - Projet
// script Sobol1.sce
// Jean-Marc Martinez Fevrier 2013
//

function y = nisp_gsobol(x,a)
    // x : a np-by-nx matrix of doubles
    // a : a 1-by-nx or nx-by-1 matrix of doubles
    np = size(x,"r") // Nombre d'experiences
    a=a(:)' // Convert into row vector
    if (np>1) then
         // Repeat the columns: a is now a np-par-nx matrix 
        a=repmat(a,np,1)
    end
    g=(abs(4*x-2)+a)./(1+a)
    y=prod(g,"c")
endfunction

function exact = nisp_sobolsa ( alpha )
    // Esperance, variance et indices exacts
    d=size(alpha,"*")
    // Calcul de la moyenne de Y
    exact.expectation = 1;

    // Calcul du carrré des coefficients de variation en fonction des alpha(i)
    cv2 = zeros(d);
    for i=1:d
        cv2(i) = 1 / (3 * (1 + alpha(i))^2);
    end

    // Calcul de la variance de Y en fonction des coefficients de variation
    // et de la moyenne 
    exact.var = (prod(cv2 + 1) - 1);

    // Calcul des indice s de sensibilité en fonction des 
    // coefficients de variation
    exact.S = zeros(d,1);
    exact.ST = zeros(d,1);
    for i=1:d
        exact.S(i) = cv2(i) / (prod(cv2+1) - 1);
        exact.ST(i) = exact.S(i) * prod(cv2+1) / (cv2(i)+1);
    end
endfunction


// Dimension et coefficients alpha
d     = 3;
//alpha = [0 1 2]; // examen
alpha = [1 2 3];  // sujet
exact = nisp_sobolsa ( alpha )

// On aurait pu définir plus directement le groupe de variables srvu
srvu = setrandvar_new(d); // groupe de d variables uniformes [0,1]

// Réalisation d'un échantillon via la méthode SRS
np = 10000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'expériences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'expériences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// Réalisation des plans A et B
ya = nisp_gsobol(A, alpha);
yb = nisp_gsobol(B, alpha);

// Estimation des indices par la methode de Sobol
mprintf("\nAnalyse de sensibilité sur le modèle gSobol\n");
mprintf("Dimension 3\n");
mprintf("Coefficients du modèle : \n");
mprintf("alpha = [%s]\n",strcat(string(alpha)," , "));
mprintf("Méthode de Sobol\n");
mprintf("\ttaille des échantillons %d\n",np);
mprintf("\tnombre d''appels au modèle %d\n",np*(d+2));
for i=1:d
  C=B;
  C(:,i) = A(:,i);
  yc = nisp_gsobol(C, alpha);

  // Calcul des indices
  rho   = corrcoef( ya , yc );
  s1    = min(1., max(0., rho));

  rho   = corrcoef( yb , yc );
  st    = min(1.,max(0., 1. - rho));

  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e (expected %12.4e)\n", s1,exact.S(i));
  mprintf("Total index is : %12.4e (expected %12.4e)\n", st,exact.ST(i));
end


