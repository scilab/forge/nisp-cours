//
// Master M2S - Projet
// script Sobol1.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique à analyser : fonction gSobol
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    y = y * (abs(4*x(i)-2)+coef(i)) / (1 + coef(i));
  end
endfunction

// Dimension et coefficients alpha
d     = 3;
//alpha = [0 1 2]; // solution
alpha = [1 2 3];  // sujet

// Calcul de la moyenne de Y
muy = 1;

// Calcul du carre des coefficients de variation en fonction des alpha(i)
cv2 = zeros(d);
for i=1:d
  cv2(i) = 1 / (3 * (1 + alpha(i))^2);
end

// Calcul de la variance de Y en fonction des coefficients de variation
// et de la moyenne 
vay = muy^2 * (prod(cv2 + 1) - 1);

// Calcul des indice s de sensibilite en fonction des 
// coefficients de variation
sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
  sx(i) = cv2(i) / (prod(cv2+1) - 1);
  sg(i) = sx(i) * prod(cv2+1) / (cv2(i)+1);
end

// On regroupe les variables
// srvu = setrandvar_new();
// for i=1:d
//   rvu(i) = randvar_new("Uniforme",0,1);
//   setrandvar_addrandvar(srvu, rvu(i));
// end
// On aurait pu definir plus directement le groupe de variables srvu
srvu = setrandvar_new(d); // groupe de d variables uniformes [0,1]

// Realisation d'un echantillon via la methode SRS
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'experiences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'experiences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// Realisation des plans A et B
for k=1:np
  ya(k) = MyFunction(A(k,:), alpha);
  yb(k) = MyFunction(B(k,:), alpha);
end

// Estimation des indices par la methode de Sobol
mprintf("\nAnalyse de sensibilite sur le modele gSobol\n");
mprintf("Dimension 3\n");
mprintf("Coefficients du modele : \n");
mprintf("alpha = [%s]\n",strcat(string(alpha)," , "));
mprintf("Methode de Sobol\n");
mprintf("\ttaille des echantillons %d\n",np);
mprintf("\tnombre d''appels au modele %d\n",np*(d+2));
for i=1:d
  C=B;
  C(:,i) = A(:,i);
  for k=1:np
    yc(k) = MyFunction(C(k,:), alpha);
  end

  // Calcul des indices
  rho   = corrcoef( ya , yc );
  s1    = min(1., max(0., rho));

  rho   = corrcoef( yb , yc );
  st    = min(1.,max(0., 1. - rho));

  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e (expected %12.4e)\n", s1,sx(i));
  mprintf("Total index is : %12.4e (expected %12.4e)\n", st,sg(i));
end


