# The method of sobol requires 2 samples.
# There are 20 factors, all following the uniform distribution on [0,1].
p <- 20
n <- 1000
X1 <- data.frame(matrix(runif(p * n), nrow = n))
X2 <- data.frame(matrix(runif(p * n), nrow = n))
# sensitivity analysis
x <- sobol2002(model = morris.fun, X1 = X1, X2 = X2, conf=0.95, nboot=100)
print(x)
plot(x)
