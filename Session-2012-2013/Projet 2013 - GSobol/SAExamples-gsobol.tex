\section{GSobol}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The function}

Consider the function \cite{SobolSaltelli1995} defined by:
\begin{eqnarray}
\label{eq-gsobol1}
g(\bx)= \prod_{i=1}^p g_i(x_i),
\end{eqnarray}
where $x\in[0,1]$ and 
\begin{eqnarray}
\label{eq-gsobol2}
g_i(x_i)=\frac{|4x_i-2|+a_i}{1+a_i},
\end{eqnarray}
where $a_i\geq 0$ are real numbers.

We consider the random variables $X_i$ independent and uniform in the 
interval $[0,1]$. 
In other words, we consider the probability distribution function 
\begin{eqnarray}
\label{eq-gsobol-pdfi}
f_i(x)=1,
\end{eqnarray}
for $x\in[0,1]$ and $f_i(x)=0$ otherwise. 

When $a_i$ is small, then the parameter $X_i$ is more influential 
on the variability of the output $Y=g(\bX)$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Properties}

The figure \ref{fig-gsobol} presents the behavior of $g$ for various 
values of $a$.
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=12cm]{gsobol-giai}
\end{center}
\caption{Function $g(x)$ for different values of a.}
\label{fig-gsobol}
\end{figure}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-gsobol2-1}
\int_0^1 g_i(x_i) dx_i = 1.
\end{eqnarray}
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray}
\label{eq-gsobol2-2}
\int_0^1 g_i(x_i) dx_i 
&=& \int_0^{0.5} g_i(x_i) dx_i + \int_{0.5}^1 g_i(x_i) dx_i 
\end{eqnarray}
The function $g_i$ is symetric with respect to $x=1/2$. 
Moreover, we have $g_i(x)\geq 0$ for any $x\in[0,1]$. 
Therefore, 
\begin{eqnarray}
\int_{0.5}^1 g_i(x_i) dx_i
&=& \int_0^{0.5} g_i(x_i) dx_i
\end{eqnarray}
Hence,
\begin{eqnarray}
\label{eq-gsobol2-2b}
\int_0^1 g_i(x_i) dx_i 
&=& 2 \int_0^{0.5} g_i(x_i) dx_i.
\end{eqnarray}

If $x\in[0,0.5]$, we have $4x-2\leq 0$, so that
\begin{eqnarray}
\int_0^{0.5} g_i(x_i) dx_i
&=& \int_0^{0.5} \frac{(2-4x_i) + a_i}{1+a_i} dx_i \\
\end{eqnarray}
However, 
\begin{eqnarray}
\int_0^{0.5} (2-4x_i) dx_i
&=& \left[ 2x_i -2x_i^2 \right]_0^{0.5} \\
&=& 2\frac{1}{2} - 2 \frac{1}{4} \\
&=& \frac{1}{2}
\end{eqnarray}

Hence, 
\begin{eqnarray}
\int_0^{0.5} g_i(x_i) dx_i
&=& \frac{1}{1+a_i} \int_0^{0.5} (2-4x + a_i)dx_i \\
&=& \frac{1}{1+a_i} \left( \int_0^{0.5} (2-4x_i)dx_i + a_i \int_0^{0.5} dx_i\right) \\
&=& \frac{1}{1+a_i} \left( \frac{1}{2} + a_i \frac{1}{2} \right) \\
&=& \frac{1}{2}.
\label{eq-gsobol2-3}
\end{eqnarray}

We plug the equations \ref{eq-gsobol2-3} 
into \ref{eq-gsobol2-2b} and get \ref{eq-gsobol2-1}.
\end{proof}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-gsobolsq-1}
\int_0^1 g_i^2(x_i) dx_i = 1 + \frac{1}{3(1+a)^2}.
\end{eqnarray}
\end{proposition}

\begin{proof}
We have
\begin{eqnarray}
\label{eq-gsobolsq-2}
\int_0^1 g_i^2(x_i) dx_i 
&=& 2 \int_0^{0.5} g_i^2(x_i) dx_i,
\end{eqnarray}
since $g_i$ is symetric with respect to $x=0.5$ and nonnegative.
Therefore, we must compute:
\begin{eqnarray}
\label{eq-gsobolsq-3}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \int_0^{0.5} \left( \frac{(2-4x)+a_i}{1+a_i} \right)^2 dx_i \\
\end{eqnarray}
However,
\begin{eqnarray}
\int_0^{0.5} (4x_i-2)^2 dx_i
&=& \int_0^{0.5} (16x_i^2 - 16x_i+4) dx_i \\
&=& \left[ \frac{16}{3} x_i^3 - 8 x_i^2 + 4x_i \right]_0^{0.5} \\
&=& \frac{16}{3} \frac{1}{8} - 8 \frac{1}{4} + 4\frac{1}{2} \\
&=& \frac{2}{3} - 2 + 2 \\
&=& \frac{2}{3}.
\end{eqnarray}
Hence, 
\begin{eqnarray}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \frac{1}{(1+a_i)^2} \int_0^{0.5} (2 - 4x_i+a_i)^2 dx_i \\
&=& \frac{1}{(1+a_i)^2} \left( \int_0^{0.5} (2-4x_i)^2 dx_i 
+ 2 a_i \int_0^{0.5} (2-4x_i) dx_i + a_i^2 \int_0^{0.5} dx_i \right)\\
&=& \frac{1}{(1+a_i)^2} \left( \frac{2}{3} + 2 a_i \frac{1}{2} + a_i^2 \frac{1}{2} \right) \\
&=& \frac{1}{(1+a_i)^2} \left( \frac{2}{3} + a_i + \frac{1}{2} a_i^2  \right)
\label{eq-gsobolsq-3b}
\end{eqnarray}
We notice that
\begin{eqnarray}
\label{eq-gsobolsq-4}
\frac{1}{2}(1+a_i)^2
&=& \frac{1}{2} + a_i + \frac{1}{2} a_i^2.
\end{eqnarray}
But $1/2+1/6=2/3$. 
Hence, 
\begin{eqnarray}
\label{eq-gsobolsq-5}
\frac{1}{6} + \frac{1}{2}(1+a_i)^2
&=& \frac{2}{3} + a_i + \frac{1}{2} a_i^2.
\end{eqnarray}
We plug the previous equation into \ref{eq-gsobolsq-3b}, and get
\begin{eqnarray}
\label{eq-gsobolsq-6}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \frac{1}{(1+a_i)^2} \left( \frac{1}{6} + \frac{1}{2}(1+a_i)^2 \right) \\
&=& \frac{1}{2} + \frac{1}{6} \frac{1}{(1+a_i)^2}.
\label{eq-gsobolsq-6b}
\end{eqnarray}
We plug the previous equation into \ref{eq-gsobolsq-2} and get \ref{eq-gsobolsq-1}, 
which concludes the proof.
\end{proof}

\begin{proposition}
\label{prop-boundgi}
We have
\begin{eqnarray}
\label{eq-gsobol2-5}
1 - \frac{1}{1+a_i} \leq g_i(x) \leq 1+\frac{1}{1+a_i},
\end{eqnarray}
for $x\in[0,1]$.
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray}
\label{eq-gsobol2-6}
g_i(x)
&=& \frac{|4x-2|-1 + 1+a_i}{1+a_i} \\
&=& 1 + \frac{|4x-2|-1}{1+a_i}.
\label{eq-gsobol2-6b}
\end{eqnarray}
Therefore, 
\begin{eqnarray}
\label{eq-gsobol2-7}
g_i(x) - 1
&=& \frac{|4x-2|-1}{1+a_i}.
\end{eqnarray}
We can now bound the absolute value of $|4x-2|-1$. 
We have  
\begin{eqnarray}
\label{eq-gsobol2-8}
|4x-2|-1 
&=& \left\{
\begin{array}{l}
(2-4x)-1 \textrm{ if } x\in[0,0.5]\\
(4x-2)-1 \textrm{ if } x\in[0.5,1].
\end{array}
\right. \\
&=& \left\{
\begin{array}{l}
1-4x \textrm{ if } x\in[0,0.5]\\
4x-3 \textrm{ if } x\in[0.5,1].
\end{array}
\right. \\
\end{eqnarray}
On one hand, $x\in[0,0.5]$ implies $1-4x\in[-1,1]$. 
On the other hand, $x\in[0.5,1]$ implies $4x-3\in[-1,1]$. 
Therefore, $|4x-2|-1 \in[-1,1]$ for $x\in[0,1]$. 
This implies 
\begin{eqnarray}
\label{eq-gsobol2-9}
-\frac{1}{1+a_i} \leq \frac{|4x-2|-1}{1+a_i} \leq \frac{1}{1+a_i} 
\end{eqnarray}
for any $a_i\geq 0$. 
We plug the previous inequality into \ref{eq-gsobol2-7} and get 
\begin{eqnarray}
\label{eq-gsobol2-10}
-\frac{1}{1+a_i} \leq g_i(x) - 1 \leq \frac{1}{1+a_i},
\end{eqnarray}
which implies the equation \ref{eq-gsobol2-5} and 
concludes the proof.
\end{proof}

The proposition \ref{prop-boundgi} can be used to see the link 
between the sensitivity of the variable $X_i$ and the value of $a_i$.
\begin{itemize}
\item if $a_i=0$, then the variable 
$x_i$ is "important", since $0\leq g_i(x)\leq 2$.
\item if $a_i=9$, then the variable 
$x_i$ is "non important", since $0.90\leq g_i(x)\leq 1.10$.
\item if $a_i=99$, then the variable 
$x_i$ is "non significant", since $0.99\leq g_i(x)\leq 1.01$.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}

The equation \ref{eq-gsobol2-1} implies
\begin{eqnarray}
\label{eq-gsobol11}
E(g_i(X_i))
&=& \int_0^1 g_i(x_i) f_i(x_i)dx_i \\
&=& \int_0^1 g_i(x_i) dx_i \\
&=& 1.
\end{eqnarray}

The expectation of $Y$ is:
\begin{eqnarray}
\label{eq-gsobol3}
E(Y)
&=& E\left(\prod_{i=1}^p g_i(x_i) \right) \\
&=& \prod_{i=1}^p E(g_i(x_i)) \\
&=& 1.
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

The equation \ref{eq-gsobolsq-1} implies 
\begin{eqnarray}
\label{eq-gsobolvar11}
E(g_i(X_i)^2)
&=& 1 + \frac{1}{3(1+a_i)^2}.
\end{eqnarray}

The variance of $Y$ is 
\begin{eqnarray}
\label{eq-gsobol4}
V(Y)=\prod_{i=1}^p \left(1+\frac{1}{3(1+a_i)^2}\right) -1.
\end{eqnarray}

Indeed, 
\begin{eqnarray}
\label{eq-gsobolvar-1}
V(Y)
&=& E(Y^2) - E(Y)^2 \\
&=& E\left(\prod_{i=1}^p g_i(x_i)^2\right) - 1 \\
&=& \prod_{i=1}^p E(g_i(x_i)^2) - 1,
\end{eqnarray}
which, combined with the equation \ref{eq-gsobolsq-1}, implies \ref{eq-gsobol11}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First order sensitivity indices}

Let $x_1\in[0,1]$. 
We have 
\begin{eqnarray}
\label{eq-gsobolS1}
E(Y|X_1=x_1)
&=&g_1(x_1)\prod_{i=2,3,...,p} E(g_i(X_i)) \\
&=&g_1(x_1).
\end{eqnarray}
Therefore, we consider the random variable 
\begin{eqnarray}
\label{eq-gsobolS1-2}
E(Y|X_1)
&=&g_1(X_1).
\end{eqnarray}
Its variance is:
\begin{eqnarray}
V(E(Y|X_1))
&=&E(E(Y|X_1)^2) - E(E(Y|X_1))^2 \\
&=&E(g_1(X_1)^2) - E(Y)^2 \\
&=& 1 + \frac{1}{3(1+a_1)^2} - 1 \\
&=& \frac{1}{3(1+a_1)^2}.
\end{eqnarray}
Hence, the first order sensitivity indice for $X_1$ is:
\begin{eqnarray}
S_1 = \frac{1}{V(Y)} \frac{1}{3(1+a_1)^2}.
\end{eqnarray}
More generally, for any $i=1,2,...,p$, 
\begin{eqnarray}
S_i = \frac{1}{V(Y)} \frac{1}{3(1+a_i)^2}.
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Total sensitivity indices}

Let $i_1,i_2,...,i_s$ be integers in the set $\{1,2,...,p\}$. 
Let $x_{i_1},x_{i_2},...,x_{i_s}$ be real numbers in the interval $[0,1]$
We have
\begin{eqnarray}
&& E(Y|X_{i_1}=x_{i_1},X_{i_2}=x_{i_2},...,X_{i_s}=x_{i_s}) \nonumber \\
&&= g_{i_1}(x_{i_1}) g_{i_2}(x_{i_2}) ... g_{i_s}(x_{i_s}) 
\prod_{\substack{i\in\{1,2,...,p\}\\ i\neq i_1,i_2,...,i_s}}
E(g_i(X_i)) \\
&&= g_{i_1}(x_{i_1}) g_{i_2}(x_{i_2}) ... g_{i_s}(x_{i_s}) 
\end{eqnarray}
Hence, we consider the random variable
\begin{eqnarray}
E(Y|X_{i_1},X_{i_2},...,X_{i_s}) 
= g_{i_1}(X_{i_1}) g_{i_2}(X_{i_2}) ... g_{i_s}(X_{i_s}) 
\end{eqnarray}
Its variance is
\begin{eqnarray}
V(E(Y|X_{i_1},X_{i_2},...,X_{i_s}))
&=& E(g_{i_1}(X_{i_1})^2 g_{i_2}(X_{i_2})^2 ... g_{i_s}(X_{i_s})^2) - E(Y)^2 \\ 
&=& \prod_{i=i_1,i_2,...,i_s} \left(1 + \frac{1}{3(1+a_i)^2}\right) - 1
\end{eqnarray}

In order to compute the total order sensitivity indice $ST_i$, 
for any $i=1,2,...,p$, we must consider all the indices different from $i$. 
However, the previous equation implies 
\begin{eqnarray}
V(E(Y|X_{\compset{i}}))
&=& \prod_{j\neq i} \left(1 + \frac{1}{3(1+a_j)^2}\right) - 1
\end{eqnarray}
Therefore, the total order sensitivity indice is
\begin{eqnarray}
ST_i
&=& 1 - \frac{\prod_{j\neq i} \left(1 + \frac{1}{3(1+a_j)^2}\right) - 1}{V(Y)}
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

The equation \ref{eq-gsobol2-6b} implies 
\begin{eqnarray}
g(\bx)
&=& \prod_{i=1}^p \left( 1 + \frac{|4x_i-2|-1}{1+a_i} \right)
\end{eqnarray}
The proposition \ref{prop-prodg} then implies
\begin{eqnarray}
g(\bx)
&=& 1+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} \frac{|4x_i-2|-1}{1+a_i} 
\end{eqnarray}
The function $g$ can then be decomposed as
\begin{eqnarray}
g(\bx)
&=& h_0+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} h_\bu(\bx_\bu),
\end{eqnarray}
where
\begin{eqnarray}
h_0 = 1
\end{eqnarray}
and
\begin{eqnarray}
\label{eq-gsoboldecomp-1}
h_\bu(\bx_\bu)
= \prod_{i\in\bu} \frac{|4x_i-2|-1}{1+a_i}.
\end{eqnarray}

The functions $h_\bu$ are excellent candidates for the Sobol' decomposition. 
In order to conclude, we have to prove that the functions $h_\bu$ satisfy 
the zero integral property. 

First, we have $E(Y)=h_0=1$, so that the constant in the 
decomposition is consistent with the Sobol' decomposition. 

Then, let $s$ be an integer in the set $\{1,2,\ldots,p\}$. 
Let $i_1,i_2,...,i_s$ be integers in the set $\{1,2,\ldots,p\}$ 
so that 
$$
1\leq i_1< i_2< \ldots< i_s\leq p.
$$
For any $k=1,2,\ldots,s$, we must prove that
\begin{eqnarray}
\label{eq-gsoboldecomp-0}
\int_0^1 h_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} = 0,
\end{eqnarray}
First, we notice that the equation \ref{eq-gsoboldecomp-1} is the same as
\begin{eqnarray}
\label{eq-gsoboldecomp-2}
h_\bu(\bx_\bu)
= \prod_{i\in\bu} (g_i(x_i)-1).
\end{eqnarray}
Therefore, 
\begin{eqnarray}
\int_0^1 h_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} 
&=& \int_0^1 \prod_{i\in\{i_1,\ldots,i_s\}} (g_i(x_i)-1) dx_{i_k}.
\end{eqnarray}
Obviously, we want to use the equation \ref{eq-gsobol2-1} 
to make the previous integral zero. 
In order to do this, we separate the indices equal to $i_k$ and 
the others. 
Indeed, 
\begin{eqnarray}
\int_0^1 g_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} 
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\int_0^1 (g_{i_k}(x_{i_k}) -1) dx_{i_k} \\
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\left( \int_0^1 g_{i_k}(x_{i_k}) dx_{i_k} - \int_0^1 dx_{i_k} \right) \\
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\left( \int_0^1 g_{i_k}(x_{i_k}) dx_{i_k} - 1 \right).
\end{eqnarray}
The equation \ref{eq-gsobol2-1} implies
\begin{eqnarray}
\int_0^1 g_{i_k}(x_{i_k}) dx_{i_k}  = 1,
\end{eqnarray}
which implies \ref{eq-gsoboldecomp-0} and concludes the proof.

