
function r = wilks(alpha,bet,n)
    // Calcule le rang r, tel que :
    // P(Y(r)>y(alpha))>bet
    // avec y(alpha) le quantile de probabilite alpha, i.e.
    // P(Y<y(alpha))=alpha.
    // Si il n'y a pas assez de données, renvoit r=0.
    // Require: specfun, distfun
    if (n < specfun_log1p(-bet)/log(alpha)) then 
        r=0;
    else
        r = distfun_binoinv(bet,n,alpha)
        r = r + 1
    end
endfunction
// mediane (alpha=0.5)
wilks(0.5,0.5,100) // => 51 // confiance 0.5
wilks(0.5,0.95,100) // => 59 // confiance 0.95
// quantile 0.95, confiance 0.95
wilks(0.95,0.95,53) // => 0 // pas assez de donnees
wilks(0.95,0.95,59) // => 59 // la valeur extrême
wilks(0.95,0.95,124) // => 122 // l'avant derniere valeur
wilks(0.95,0.95,153) // => 150 ...
