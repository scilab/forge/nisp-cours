// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.

// Reference
// http://en.wikipedia.org/wiki/Gumbel_distribution
//
// NIST/SEMATECH e-Handbook of Statistical Methods, 
// http://www.itl.nist.gov/div898/handbook/
// http://www.itl.nist.gov/div898/handbook/eda/section3/eda366g.htm

// Gumbel distribution (minimum)
// mu: location
// sigma: scale

function y = evpdf(x,mu,sigma)
    // Gumbel (minimum) probability distribution function
    // To get the max-Gumbel PDF:
    // y = evpdf(-x,-mu,sigma)
    z=(x-mu)/sigma
    y=exp(z-exp(z))/sigma
endfunction

// Plot the Gumbel PDF
N=1000;
x=linspace(-20,5,N);
y1= evpdf(x,-0.5,2.);
y2= evpdf(x,-1.0,2.);
y3= evpdf(x,-1.5,3.);
y4= evpdf(x,-3.0,4.);
scf();
xtitle("Gumbel","x","Density");
plot(x,y1,"r-")
plot(x,y2,"g-")
plot(x,y3,"b-")
plot(x,y4,"c-")
leg(1)="$\mu=-0.5,\beta=2.0$";
leg(2)="$\mu=-1.0,\beta=2.0$";
leg(3)="$\mu=-1.5,\beta=3.0$";
leg(4)="$\mu=-3.0,\beta=4.0$";
legend(leg,"in_upper_left");

// Check the integral
mu=-0.5;
sigma=2;
intg(-50,10,list(evpdf,mu,sigma))

//////////////////////////////////////////////////////////////////

function p = evcdf(x,mu,sigma,lowertail)
    // Gumbel cumulated distribution function
    // If (lowertail), returns P(X<x)
    // otherwise, returns P(X>x).
    // To get the max-Gumbel CDF:
    // y = evcdf(-x,-mu,sigma,lowertail)
    z=(x-mu)./sigma
    if (lowertail) then
        p=-specfun_expm1(-exp(z))
    else
        p=exp(-exp(z))
    end
endfunction

// Plot the Gumbel CDF
N=1000;
x=linspace(-20,5,N);
p1= evcdf(x,-0.5,2.,%t);
p2= evcdf(x,-1.0,2.,%t);
p3= evcdf(x,-1.5,3.,%t);
p4= evcdf(x,-3.0,4.,%t);
scf();
xtitle("Gumbel","x","P(X<x)");
plot(x,p1,"r-")
plot(x,p2,"g-")
plot(x,p3,"b-")
plot(x,p4,"c-")
leg(1)="$\mu=-0.5,\beta=2.0$";
leg(2)="$\mu=-1.0,\beta=2.0$";
leg(3)="$\mu=-1.5,\beta=3.0$";
leg(4)="$\mu=-3.0,\beta=4.0$";
legend(leg,"in_upper_left");

p= evcdf(1,0.5,2.,%t)
q= evcdf(1,0.5,2.,%f)
p+q

// Check the derivative
x=-1;
mu=-0.5;
sigma=2;
f1=derivative(list(evcdf,mu,sigma,%t),x)
f=evpdf(x,mu,sigma)
assert_computedigits(f,f1)

//////////////////////////////////////////////////////////////////

function R=evrnd(mu,sigma,m,n)
    // Gumbel random numbers
    // To get max-Gumbel random numbers:
    // y = -evrnd(-mu,sigma,m,n)
    u=distfun_unifrnd(0,1,m,n)
    R=mu+sigma*log(-log(u))
endfunction

// Plot Gumbel random numbers
N=1000;
x=linspace(-20,5,N);
y1= evpdf(x,0.5,2.);
R=evrnd(0.5,2,10000,1);
scf();
xtitle("Gumbel distribution","x","Density");
plot(x,y1)
histplot(20,R);
legend(["PDF","Data"],"in_upper_left");

// Compare with CDF
R=gsort(R,"g","i");
n=size(R,"*");
p=evcdf(R,0.5,2,%t);
scf();
plot(R,(1:n)'/n,"r-");
plot(R,p,"b-");
legend(["Empirical","CDF"],"in_upper_left");
xtitle("Gumbel distribution","x","P(X<x)");

//////////////////////////////////////////////////////////////////

function x=evinv(p,mu,sigma,lowertail)
    // Gumbel (minimum) inverse cumulated distribution function
    // To get the max-Gumbel Inverse CDF:
    // x=-evinv(p,-mu,sigma,lowertail)
    if (lowertail) then
        z=log(-specfun_log1p(-p))
    else
        z=log(-log(p))
    end
    x=mu+sigma*z
endfunction
x=1;
mu=0.5;
sigma=2.;
lowertail=%t;
p=evcdf(x,mu,sigma,lowertail);
x1=evinv(p,mu,sigma,lowertail)
assert_computedigits(x,x1)
//
lowertail=%f;
p=evcdf(x,mu,sigma,lowertail);
x1=evinv(p,mu,sigma,lowertail)
assert_computedigits(x,x1)

//////////////////////////////////////////////////////////////////

function [M,V]=evstat(mu,sigma)
    // Gumbel statistics
    z=dlgamma(1)
    M=mu+sigma*z
    V=%pi^2*sigma^2/6
endfunction

mu=0.5;
sigma=2.0;
[M,V]=evstat(mu,sigma);
R=evrnd(mu,sigma,1000,1000);
m=mean(R,"r");
v=variance(R,"r");
// 
scf();
xtitle("Estimate of mean","Mean","Frequency")
histplot(11,m);
plot([M,M],[0,5])
legend(["Data","Exact"]);
// 
scf();
xtitle("Estimate of variance","Variance","Frequency")
histplot(11,v);
plot([V,V],[0,1])
legend(["Data","Exact"]);

//////////////////////////////////////////////////////////////////
// Let X maximum of n random variables with normal distribution.
// When n increases, the distribution of X becomes 
// closer and closer to the Gumbel distribution.
// Reference
// http://www.panix.com/~kts/Thesis/extreme/extreme2.html
//

stacksize("max");
N=2000;
mu=0;
sigma=1;
x=linspace(0,6,100);
scf();
xtitle("Max. of n Normal variables","X","Frequency")
//
n=10;
R=distfun_normrnd(mu,sigma,n,N);
X=max(R,"r");
histplot(50,X,style=2);
b = distfun_norminv(1-1/n,mu,sigma);
a = distfun_norminv(1-1/(n*exp(1)),mu,sigma) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");
//
n=100;
R=distfun_normrnd(mu,sigma,n,N);
X=max(R,"r");
histplot(50,X,style=3);
b = distfun_norminv(1-1/n,mu,sigma);
a = distfun_norminv(1-1/(n*exp(1)),mu,sigma) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");
//
n=1000;
R=distfun_normrnd(mu,sigma,n,N);
X=max(R,"r");
histplot(50,X,style=4);
legend(["n=10","n=100","n=1000"]);
b = distfun_norminv(1-1/n,mu,sigma);
a = distfun_norminv(1-1/(n*exp(1)),mu,sigma) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");

//////////////////////////////////////////////////////////////////
// Let X maximum of n random variables with exponential distribution.
// When n increases, the distribution of X becomes 
// closer and closer to the Gumbel distribution.
// Reference
// http://www.panix.com/~kts/Thesis/extreme/extreme2.html
//

stacksize("max");
N=2000;
mu=20;
x=linspace(0,300,100);
scf();
xtitle("Max. of n Exp variables","X","Frequency")
//
n=10;
R=distfun_exprnd(mu,n,N);
X=max(R,"r");
histplot(50,X,style=2);
b = distfun_expinv(1-1/n,mu);
a = distfun_expinv(1-1/(n*exp(1)),mu) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");
//
n=100;
R=distfun_exprnd(mu,n,N);
X=max(R,"r");
histplot(50,X,style=3);
b = distfun_expinv(1-1/n,mu);
a = distfun_expinv(1-1/(n*exp(1)),mu) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");
//
n=1000;
R=distfun_exprnd(mu,n,N);
X=max(R,"r");
histplot(50,X,style=4);
legend(["n=10","n=100","n=1000"]);
b = distfun_expinv(1-1/n,mu);
a = distfun_expinv(1-1/(n*exp(1)),mu) - b;
y=evpdf(-x,-b,a);
plot(x,y,"k");
