\section{Sum}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The function}

Consider the function 
\begin{eqnarray}
\label{eq-gsum1}
g(X) = a_1 X_1 + a_2 X_2 + ... + a_p X_p + a_{p+1}
\end{eqnarray}
where $X_1, X_2, ..., X_p \in\RR$ are independent random variables 
with means $\mu_1, \mu_2, ..., \mu_p$ and variances 
$\sigma_1^2, \sigma_2^2, ..., \sigma_p^2$ 
and $a_i\in\RR$ for $i=1,2,...,p+1$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}

The expected value of $Y$ is:
\begin{eqnarray*}
E(Y) 
&=& E\left(a_1 X_1 + a_2 X_2 + ... + a_p X_p + a_{p+1}\right) \\
&=& a_1 E(X_1) + a_2 E(X_2) + ... + a_p E(X_p) + a_{p+1} \\
&=& a_1 \mu_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1}.
\end{eqnarray*}
The previous result can be obtained even if the variables 
$X_i$ are dependent. 
For details on this classical result, see, for example, \cite{Ross1987}, 
chapter 4 "Random Variables and Expectation", 
section "Expected Value of Sums of Random Variables".

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

The variance of $Y$ is:
\begin{eqnarray*}
V(Y) 
&=& V\left(a_1 X_1 + a_2 X_2 + ... + a_p X_p\right) \\
&=& a_1^2 V(X_1) + a_2^2 V(X_2) + ... + a_p^2 V(X_p) \\
&=& a_1^2 \sigma_1^2 + a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2.
\end{eqnarray*}
The second equality is true because the variables $X_i$ are 
independent. 
For details on this classical result, see, for example, \cite{Ross1987}, 
chapter 4 "Random Variables and Expectation", 
section "Covariance and variance of sums of random variables".

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First order sensitivity indices}

Let $x_1$ be a real. 
The expected value of $Y$ given $X_1=x_1$ is:
\begin{eqnarray*}
E(Y|X_1=x_1) 
&=& E\left(a_1 x_1 + a_2 X_2 + ... + a_p X_p + a_{p+1}\right) \\
&=& a_1 x_1 + a_2 E(X_2) + ... + a_p E(X_p) + a_{p+1} \\
&=& a_1 x_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1}.
\end{eqnarray*}
The variance $V(E(Y|X_1=x_1))$ is 
\begin{eqnarray*}
V(E(Y|X_1))
&=& V\left(a_1 X_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1}\right) \\
&=& a_1^2 V(X_1) + 0 + ... + 0 \\
&=& a_1^2 \sigma_1^2.
\end{eqnarray*}
The second equality is because $V(\mu_2)=V(\mu_3)=...=V(\mu_p)=0$, 
since the means are constants.
The first order sensitivity indice associated to $X_1$ is:
\begin{eqnarray*}
S_1 
&=& \frac{V(E(Y|X_1))}{V(Y)} \\
&=& \frac{a_1^2 \sigma_1^2}{a_1^2 \sigma_1^2 + a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2}
\end{eqnarray*}
More generally, we have
\begin{eqnarray*}
S_i 
&=& \frac{a_i^2 \sigma_i^2}{a_1^2 \sigma_1^2 + a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2},
\end{eqnarray*}
for $i=1,2,...,p$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Total sensitivity indices}

We have to compute $E(Y|\bX_{\compset{1}})$. 
The set of indices $\compset{1}$ is $\{2,3,...,p\}$. 
Let $x_2$, $x_3$ ... $x_p$ be real numbers. 
The expected value of $Y$ given $X_2=x_2$, $X_3=x_3$, ..., $X_p=x_p$ is:
\begin{eqnarray*}
E(Y|\bX_{\compset{1}}=(x_2,x_3,...,x_p)) 
&=& E\left(a_1 X_1 + a_2 x_2 + a_3 x_3 + ... + a_p x_p + a_{p+1}\right) \\
&=& a_1 E(X_1) + a_2 x_2 + a_3 x_3 + ... + a_p x_p + a_{p+1} \\
&=& a_1 \mu_1 + a_2 x_2 + a_3 x_3 + ... + a_p x_p + a_{p+1} \\
\end{eqnarray*}
Therefore the variance $V(E(Y|\bX_{\compset{1}}))$ is 
\begin{eqnarray*}
V(E(Y|\bX_{\compset{1}})) 
&=& V\left(a_1 \mu_1 + a_2 X_2 + a_3 X_3 + ... + a_p X_p + a_{p+1}\right) \\
&=& 0 + a_2^2 V(X_2) + a_3^2 V(X_3) + ... +a_p^2 V(X_p) \\
&=& a_2^2 \sigma_2^2 + a_3^2 \sigma_3^2 + ... + a_p^2 \sigma_p^2
\end{eqnarray*}
The second equality is true because the variables $X_i$ are 
independent. 
Therefore, the total sensitivity indice associated with $X_1$ is:
\begin{eqnarray*}
ST_1 &=& 1 - \frac{V(E(Y|\bX_{\compset{1}}))}{V(Y)} \\
&=& 1 - \frac{a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2}{
a_1^2 \sigma_1^2 + ... + a_p^2 \sigma_p^2} \\
&=& \frac{(a_1^2 \sigma_1^2 + a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2) - (a_2^2 \sigma_2^2 + ... + a_p^2 \sigma_p^2)}{
a_1^2 \sigma_1^2 + ... + a_p^2 \sigma_p^2} \\
&=& \frac{a_1^2 \sigma_1^2 }{a_1^2 \sigma_1^2 + ... + a_p^2 \sigma_p^2}
\end{eqnarray*}
We notice that $ST_1=S_1$. More generally, we have:
\begin{eqnarray*}
ST_i &=& S_i,
\end{eqnarray*}
for $i=1,2,...,p$.

This implies that there are no interactions between the variables. 
This implies that the higher order indices (e.g. $S_{ij}$ for $i\neq j$) 
are zero. 
But this could have been seen right from start, given that the model 
is affine. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

The first function in the decomposition is:
\begin{eqnarray*}
h_0
&=&E(Y) \\
&=&a_1 \mu_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1}.
\end{eqnarray*}

Let $x_1\in\RR$. 
We are searching the first function $g_1(x_1)$ in the decomposition. 
This functions is:
\begin{eqnarray*}
h_1(x_1)
&=&E(Y|X_1=x_1) - E(Y) \\
&=&a_1 x_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1} - (a_1 \mu_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1}). \\
&=&a_1 (x_1-\mu_1).
\end{eqnarray*}
More generally, we have
\begin{eqnarray*}
h_i(x_i)
&=&a_i (x_i - \mu_i),
\end{eqnarray*}
for $i=1,2,...,p$. 

We may compute directly the higher order functions, such as, 
for example, $h_{1,2}(x_1,x_2)$. 
As a matter of fact, these functions are zero, as we are 
going to check. 
Indeed, 
\begin{eqnarray*}
h_0 + h_1(x_1) + ... +  h_p(x_p)
&=&a_1 \mu_1 + a_2 \mu_2 + ... + a_p \mu_p + a_{p+1} + \nonumber \\
&& a_1 (x_1 - \mu_1) + ... + a_p (x_p - \mu_p) \\
&=& a_1 x_1 + ... + a_p x_p + a_{p+1} \\
&=& g(\bx).
\end{eqnarray*}
Hence, the functions $h_0,h_1,...,h_p$ are the Sobol' decomposition of the 
function $g$ and the remaining functions are zero. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical experiments}

We have performed the sensitivity analysis with 
Sobol' method and with polynomial chaos on the previous function. 
We have used $p=3$ and the parameters :
$$
a = (1,2,4,0), \quad 
\mu=(0,0,0), \quad 
\sigma=(1,2,4).
$$

% The following script performs the sensitivity analysis with Scilab.

% \lstset{language=scilabscript}
% \lstinputlisting{scripts/test-sum.sce}

%The previous script produces the following output.

The following session shows a comparison of the 
Martinez' estimate with 10000 experiments with the exact values. 
\lstset{language=scilabscript}
\begin{lstlisting}
With Sobol' method.
S(1)=0.041230 (exact=0.003663)
ST(1)=0.003749 (exact=0.003663)
S(2)=0.077191 (exact=0.058608)
ST(2)=0.060096 (exact=0.058608)
S(3)=0.934254 (exact=0.937729)
ST(3)=0.937347 (exact=0.937729)
\end{lstlisting}

The following session presents the polynomial chaos 
analysis based on a degree 6 polynomial. 
\lstset{language=scilabscript}
\begin{lstlisting}
Polynomials Chaos
Mean(Y)     = 0.000000 (exact=0.000000)
Variance(Y) = 273.000000(exact=273.000000)
S(1)=0.003663 (exact=0.003663)
S(2)=0.058608 (exact=0.058608)
S(3)=0.937729 (exact=0.937729)
ST(1)=0.003663 (exact=0.003663)
ST(2)=0.058608 (exact=0.058608)
ST(3)=0.937729 (exact=0.937729)
#Exact Digits S(1):15
#Exact Digits ST(1):15
#Exact Digits S(2):14
#Exact Digits ST(2):14
#Exact Digits S(3):15
#Exact Digits ST(3):15
\end{lstlisting}

The previous script also produces the following figures. 

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/test-sum-gplot}
\end{center}
\caption{Sum test case - Function $g(x)$ for different values of a.}
\label{fig-sumga}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/test-sum-histo}
\end{center}
\caption{Sum test case - Histogram of $Y$ for 1000 samples.}
\label{fig-sumhisto}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/test-sum-scatter.png}
\end{center}
\caption{Sum test case - Scatter plot of $Y$ for 1000 samples.}
\label{fig-sumscatterplot}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/test-sum-sobolmethod}
\end{center}
\caption{Sum test case - Martinez' method. Number of significant digits 
for an increasing number of simulations.}
\label{fig-sumsobolmethod}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/test-sum-pcmethod}
\end{center}
\caption{Sum test case - Polynomial chaos method. Number of significant digits 
for an increasing polynomial degree.}
\label{fig-sumpcmethod}
\end{figure}

The figure \ref{fig-sumga} shows that the function is linear 
with respect to its arguments, with a slope with increases 
as $a_i$ increases. 
This can also be seen from the scatter plot in the figure \ref{fig-sumscatterplot}. 
Moreover, from the scatter plot, we can immediately rank the variables 
with respect to their influence to the variability of $Y$: $X_3>X_2>X_1$. 
This is confirmed by the sensitivity analysis, which also shows that the 
first order indices are equal to the total indices. 
This implies that there is no interaction between the variables. 
This is consistent with the fact that the model is affine. 

The Sobol' method performs well in this case and seems to converge to 
the exact indices, as shown in the figure \ref{fig-sumsobolmethod}. 
The number of significant digits seems to increase from 1 to 3 digits. 
However, the estimates for $S_1$ are not accurate for the number 
of samples useds in this experiment. 
Indeed, the number of significant digits is always zero. 
The reason for this is that the exact value of $S_1=0.003663$ is small 
in magnitude, so that the method has problems to estimate it.

On the other hand, the sensitivity analysis based on the chaos polynomials 
is exact, as is shown by the number of correct digits which is larger than 
14 (the maximum possible is 17). 
This result remains true whatever the degree of the polynomials, 
provided that we use a degree larger than 1, as shown in the 
figure \ref{fig-sumpcmethod}.
This is consistent with the fact that the model is affine, 
so that the polynomials have no problem to approximate this function. 

