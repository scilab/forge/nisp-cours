\section{Product}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The function}


Consider the function 
\begin{eqnarray*}
g(\bX) = X_1 X_2 ... X_p
\end{eqnarray*}
where $X_1, X_2, ..., X_p$ are independent random variables 
with means $\mu_1$, $\mu_2$, ..., $\mu_p$ and variances 
$\sigma_1^2$, $\sigma_2^2$, ..., $\sigma_p^2$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}

The expectation of $Y$ is 
\begin{eqnarray*}
E(Y)
&=& \int_{\RR^p} x_1 ... x_p f_{1,...,p}(\bx) d\bx \\
&=& \left(\int_\RR x_1 f_1(x_1) dx_1 \right) ... \left(\int_\RR x_p f_p(x_p) dx_p \right) \\
&=& E(X_1)...E(X_p) \\
&=& \mu_1 ...\mu_p.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

The variance of $Y$ is 
\begin{eqnarray*}
V(Y)
&=& E(Y^2) - E(Y)^2 \\
&=& E(X_1^2)...E(X_p^2) - E(Y)^2 \\
&=& (V(X_1)+E(X_1)^2)...(V(X_p)+E(X_p)^2) - E(Y)^2 \\
&=& (\sigma_1^2+\mu_1^2)...(\sigma_p^2+\mu_p^2) - \mu_1^2 ...\mu_p^2.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First order sensitivity indices}

Let $x_1\in\RR$ be an outcome of the random variable $X_1$. 
The conditional expectation of $Y$ given $X_1=x_1$ is
\begin{eqnarray*}
E(Y|X_1=x_1)
&=& E(x_1 X_2...X_p) \\
&=& x_1 E(X_2...X_p) \\
&=& x_1 \mu_2...\mu_p.
\end{eqnarray*}
Therefore, we must consider the random variable
\begin{eqnarray*}
\label{eq-gproduct5}
E(Y|X_1)
&=& X_1 \mu_2...\mu_p.
\end{eqnarray*}
Its variance is
\begin{eqnarray*}
V(E(Y|X_1))
&=& E(E(Y|X_1)^2) - E(Y)^2 \\
&=& E(X_1^2 \mu_2^2...\mu_p^2) - \mu_1^2...\mu_p^2 \\
&=& E(X_1^2) \mu_2^2...\mu_p^2 - \mu_1^2...\mu_p^2 \\
&=& (V(X_1) + E(X_1)^2) \mu_2^2...\mu_p^2 - \mu_1^2 \mu_2^2 ...\mu_p^2 \\
&=& (\sigma_1^2+\mu_1^2) \mu_2^2...\mu_p^2 - \mu_1^2 \mu_2^2...\mu_p^2 \\
&=& (\sigma_1^2+\mu_1^2 - \mu_1^2) \mu_2^2...\mu_p^2 \\
&=& \sigma_1^2 \mu_2^2...\mu_p^2.
\end{eqnarray*}
More generally, 
\begin{eqnarray*}
\label{eq-gproduct7}
V(E(Y|X_i))
&=& \sigma_i^2 \prod_{\substack{j=1,...,p\\j\neq i}}\mu_j^2.
\end{eqnarray*}
Hence, the first order sensitivity indice is
\begin{eqnarray*}
S_i
&=& \frac{\sigma_i^2 \prod_{\substack{j=1,...,p\\j\neq i}}\mu_j^2}{V(Y)}.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Total sensitivity indices}

Let $x_1\in\RR$ be an outcome of the random variable $X_1$. 
The conditional expectation of $Y$ given $X_{\compset{1}}=x_{\compset{1}}$ is
\begin{eqnarray*}
E(Y|X_{\compset{1}}=x_{\compset{1}})
&=& E(Y|X_2=x_2,...,X_p=x_p) \\
&=& E(X_1 x_2 ... x_p) \\
&=& E(X_1) x_2 ... x_p \\
&=& \mu_1 x_2 ... x_p.
\end{eqnarray*}
Hence, we consider the random variable
\begin{eqnarray*}
E(Y|X_{\compset{1}})
&=& \mu_1 X_2 ... X_p.
\end{eqnarray*}
Its variance is
\begin{eqnarray*}
V(E(Y|X_{\compset{1}})
&=& E(E(Y|X_{\compset{1}})^2) - E(Y)^2 \\
&=& E(\mu_1^2 X_2^2 ... X_p^2) - \mu_1^2 ...\mu_p^2 \\
&=& \mu_1^2 E(X_2^2) ... E(X_p^2) - \mu_1^2 ...\mu_p^2 \\
&=& \mu_1^2 (\sigma_2^2+\mu_2^2) ... (\sigma_p^2+\mu_p^2) - \mu_1^2 ...\mu_p^2.
\end{eqnarray*}
More generally, for any $i=1,2,...,p$, we have
\begin{eqnarray*}
V(E(Y|X_{\compset{i}})
&=& \mu_i^2 \prod_{\substack{j=1,...,p\\ j\neq i}} (\sigma_j^2+\mu_j^2) - \mu_1^2 ...\mu_p^2.
\end{eqnarray*}
Therefore, the total sensitivity indice is
\begin{eqnarray*}
ST_i
&=& 1 - \frac{\mu_i^2 \prod_{\substack{j=1,...,p\\ j\neq i}} (\sigma_j^2+\mu_j^2) - \mu_1^2 ...\mu_p^2}{V(Y)}.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

The first function in the decomposition is
\begin{eqnarray*}
h_0
&=& E(Y) \\
&=& \mu_1 ...\mu_p.
\end{eqnarray*}
The first order Sobol' functions are:
\begin{eqnarray*}
h_i(x_i)
&=& E(Y|X_i=x_i) - E(Y) \\
&=& x_i \mu_2...\mu_p - \mu_1 ...\mu_p \\
&=& (x_i-\mu_i) \mu_2...\mu_p.
\end{eqnarray*}

The remaining functions of the decomposition are nonzero, 
and represent the various interactions between the variables. 
But we were not able to find a simple, generic, way of expressing 
these functions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical experiments}

We have performed the sensitivity analysis 
with $p=3$ variables and the following parameters:
$$
\mu=(4,2,1), \quad 
\sigma=(1,2,4).
$$
% The following script performs the sensitivity analysis with Scilab.

% \lstset{language=scilabscript}
% \lstinputlisting{scripts/test-product.sce}

%The previous script produces the following output.

The following session shows a comparison of the 
Martinez' method with 1000 experiments with the exact values. 
\lstset{language=scilabscript}
\begin{lstlisting}
S(1)=0.000000 (exact=0.001779)
ST(1)=0.062404 (exact=0.060498)
S(2)=0.000000 (exact=0.028470)
ST(2)=0.572773 (exact=0.514235)
S(3)=0.475323 (exact=0.455516)
ST(3)=1.000000 (exact=0.967972)
\end{lstlisting}

The following session presents the polynomial chaos 
analysis based on a degree 6 polynomial.
\lstset{language=scilabscript}
\begin{lstlisting}
Mean(Y)     = 8.000000 (exact=8.000000)
Variance(Y) = 2248.000000(exact=2248.000000)
S(1)=0.001779 (exact=0.001779)
S(2)=0.028470 (exact=0.028470)
S(3)=0.455516 (exact=0.455516)
ST(1)=0.060498 (exact=0.060498)
ST(2)=0.514235 (exact=0.514235)
ST(3)=0.967972 (exact=0.967972)
#Exact Digits S(1):14
#Exact Digits ST(1):14
#Exact Digits S(2):14
#Exact Digits ST(2):14
#Exact Digits S(3):14
#Exact Digits ST(3):14
\end{lstlisting}

The previous script also produces the following figures. 

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/product-histo}
\end{center}
\caption{Product test case - Histogram of $Y$ for 1000 samples.}
\label{fig-prodhisto}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/product-scatter.png}
\end{center}
\caption{Product test case - Scatter plot of $Y$ for 1000 samples.}
\label{fig-prodscatterplot}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/test-sum-sobolmethod}
\end{center}
\caption{Product test case - Martinez' method. Convergence of the sensitivity indices 
for an increasing number of simulations.}
\label{fig-prodsobolmethod}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/product-pcmethod}
\end{center}
\caption{Product test case - Polynomial chaos method. Number of 
significant digits of the sensitivity indices for an increasing polynomial degree.}
\label{fig-prodpcmethod}
\end{figure}

The sensitivity indices shows that we can rank the variables 
with the order $X_3>X_2>X_1$. 
However, there is a significant difference between the first order and the 
total sensitivity indices. 
This implies that the interactions between the variables has a significant 
effect on the variability of the output. 
For example, for the most important variable, $X_3$, the first order sensitivity 
indice is only 0.45 while the total sensitivity indice is 0.96. 
This shows that this model is sensitive to the interactions between the variables. 
This is consistent with the fact that the function is a product, so that 
all the functions in the Sobol' ANOVA decomposition are nonzero.

The figure \ref{fig-prodsobolmethod} shows that that the Sobol' method 
performs well on this test case, since the estimates seem to converge to 
their exact value, with an absolute error from $10^{-1}$ to $10^{-3}$, 
associated with a number of digits from 1 to 3. 
However, the accuracy for the estimate of $S_1=0.001779$ is so low 
that the number of significant digits is zero. 
The reason for this is probably that the sensitivity indice is small in 
magnitude.

The figure \ref{fig-prodpcmethod} shows that the chaos polynomials 
perform extremely well on this case, provided that the degree of the polynomial 
is greater than 3. 
If this is true, then the representation is almost exact and the sensitivity 
indices have from 14 to 16 significant digits (the maximum possible is 17). 
Indeed, the polynomial degree $d=3$ corresponds to the number of variables, 
which is the maximum number of variables involved in the interaction. 



