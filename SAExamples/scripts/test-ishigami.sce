// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Test the chaos polynomial decomposition on the Ishigami function.
// Three uniform variables in [-pi,pi].
// Create a Petras sampling and compute the coefficients of the polynomial 
// by integration.
//
// Requires : Stixbox (histo)
//

// 3 random variable uniform in [-pi,pi]
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );

// 1. Create a group of stochastic variables
nx = 3;
srvx = setrandvar_new( nx );

// 2. Create a group of uncertain variables
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 3. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
inputdata = setrandvar_buildsample( srvu , srvx );

// 4. Create the chaos polynomial
pc = polychaos_new ( srvx , 1 );
polychaos_setdegree(pc,degre);

// 5. Perform the experiments
np = setrandvar_getsize(srvu);
mprintf("Number of experiments = %d\n",np);
polychaos_setsizetarget(pc,np);
outputdata = nisp_ishigami(inputdata,a,b);
polychaos_settarget(pc,outputdata);

// 6. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,"Integration");

// 7. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);

// Print summary
polychaos_sasummary(pc);

// Consider the group {X1,X2}
groupe = [1 3];
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , groupe(1) );
polychaos_setgroupaddvar ( pc , groupe(2) );
mprintf("Group {X1,X3}\n");
ST13=exact.S1+exact.S3+exact.S13;
mprintf("    ST13 =%f (expected=%f)\n",polychaos_getgroupind(pc),ST13);
mprintf("    S13  =%f (expected=%f)\n",polychaos_getgroupinter(pc),exact.S13);

// Compute the histogram of the output of the chaos polynomial.
polychaos_buildsample(pc,"Lhs",10000);
sample_output = polychaos_getsample(pc);
h = scf();
histo(sample_output,"Normalization","pdf")
xlabel("Y");
ylabel("Frequency");
xtitle("Ishigami - Histogram");
h.children.ticks_format(2)="%.2f";

// Plot the sensitivity indices.
for i=1:nx
    indexfirst(i)=polychaos_getindexfirst(pc,i);
    indextotal(i)=polychaos_getindextotal(pc,i);
end
h = scf();
bar(indextotal,0.2,'blue');
bar(indexfirst,0.15,'yellow');
legend(["Total" "First order"],pos=1);
xtitle("Ishigami - Sensitivity index");
h.children.ticks_format(2)="%.1f";
//
// Clean-up
//
randvar_destroy ( rvu1 );
randvar_destroy ( rvu2 );
randvar_destroy ( rvu3 );
setrandvar_destroy ( srvu );
polychaos_destroy ( pc );
setrandvar_destroy ( srvx );
//
// Analysis of the estimates
mprintf("Analysis of the estimates\n")
sxerror=[];
sterror=[];
degremax = 25;
degreearray=[];
narray=[];
exactS=[exact.S1,exact.S2,exact.S3];
exactST=[exact.ST1,exact.ST2,exact.ST3];
for degre = 1 : degremax
    degreearray(degre)=degre;
    nx = 3;
    srvx = setrandvar_new( nx );
    // 2. Create a group of uncertain variables
    rvu1 = randvar_new("Uniforme",-%pi,%pi);
    rvu2 = randvar_new("Uniforme",-%pi,%pi);
    rvu3 = randvar_new("Uniforme",-%pi,%pi);
    srvu = setrandvar_new();
    setrandvar_addrandvar ( srvu, rvu1);
    setrandvar_addrandvar ( srvu, rvu2);
    setrandvar_addrandvar ( srvu, rvu3);
    // 3. Create a Petras sampling
    setrandvar_buildsample(srvx,"Quadrature",degre);
    inputdata = setrandvar_buildsample( srvu , srvx );
    // 4. Create the chaos polynomial
    pc = polychaos_new ( srvx , 1 );
    polychaos_setdegree(pc,degre);
    // 5. Perform the experiments
    np = setrandvar_getsize(srvu);
    polychaos_setsizetarget(pc,np);
    outputdata = nisp_ishigami(inputdata,a,b);
    polychaos_settarget(pc,outputdata);
    // 6. Compute the coefficients by integration
    polychaos_computeexp(pc,srvx,"Integration");
    // Plot the sensitivity indices.
    S = polychaos_getindexfirst(pc);
    ST = polychaos_getindextotal(pc);
    nisp_destroyall ();
    for i=1:nx
        sxerror(degre,i)=assert_computedigits(S(i),exactS(i));
        sterror(degre,i)=assert_computedigits(ST(i),exactST(i));
    end
    narray(degre)=size(inputdata,"r");
end
h=scf();
xtitle("","Degree of the P.C.","Number of digits")
plot(degreearray,sxerror(:,1),"r-")
plot(degreearray,sxerror(:,2),"g-")
plot(degreearray,sxerror(:,3),"b-")
plot(degreearray,sterror(:,1),"r-.")
plot(degreearray,sterror(:,2),"g-.")
plot(degreearray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"],"in_upper_left");
//
h=scf();
xtitle("","Sample size","Number of digits")
plot(narray,sxerror(:,1),"r-")
plot(narray,sxerror(:,2),"g-")
plot(narray,sxerror(:,3),"b-")
plot(narray,sterror(:,1),"r-.")
plot(narray,sterror(:,2),"g-.")
plot(narray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"],"in_upper_left");
h.children(1).log_flags="lnn";
