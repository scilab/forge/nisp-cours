// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// See the convergence of S and ST with 
//

function x=myrandgen(m, i)
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b )
nx = 3;
exactS=[exact.S1,exact.S2,exact.S3];
exactST=[exact.ST1,exact.ST2,exact.ST3];
// See the convergence of the sensitivity indices
stacksize("max");
narray=[];
sxerror=[];
sterror=[];
narray(1)=100;
method="Saltelli";
for k = 1 : 100
    if (k>1) then
        narray(k) = ceil(1.5*narray(k-1));
    end    
    tic();
    s=nisp_sobolsaFirst(list(nisp_ishigami,a,b),nx,myrandgen,narray(k),[],[],method);
    st=nisp_sobolsaTotal(list(nisp_ishigami,a,b),nx,myrandgen,narray(k),[],[],method);
    t = toc();
    mprintf("Run #%d, n=%d, t=%.2f (s)\n",k,narray(k),t);
    for i=1:nx
        sxerror(k,i)=assert_computedigits(s(i),exactS(i));
        sterror(k,i)=assert_computedigits(st(i),exactST(i));
    end
    if ( t > 5 ) then
        break
    end
end
h = scf();
plot(narray,sxerror(:,1),"b-");
plot(narray,sxerror(:,2),"r-");
plot(narray,sxerror(:,3),"g-");
plot(narray,sterror(:,1),"b-.");
plot(narray,sterror(:,2),"r-.");
plot(narray,sterror(:,3),"g-.");
mytitle=msprintf("Function : Ishigami, Estimator : %s",method);
title(mytitle)
xlabel("Number of simulations");
ylabel("Number of exact digits");
legend(["S1","S2","S3","ST1","ST2","ST3"]);
h.children(1).log_flags="lnn";
