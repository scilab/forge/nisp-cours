// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Test the chaos polynomial decomposition on the Ishigami function.
// Three uniform variables in [-pi,pi].
// Create a Petras sampling and compute the coefficients of the polynomial 
// by integration.
//
// Shows how to use the chaos polynomial meta-model approximating the Ishigami function.
//

function y = ishigami (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  a=7.
  b=0.1
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction

// 1. Create a group of stochastic variables
nx = 3;
rvx1 = randvar_new("Uniforme");
rvx2 = randvar_new("Uniforme");
rvx3 = randvar_new("Uniforme");

srvx = setrandvar_new( );
setrandvar_addrandvar ( srvx , rvx1 );
setrandvar_addrandvar ( srvx , rvx2 );
setrandvar_addrandvar ( srvx , rvx3 );

// 2. Create a group of uncertain variables
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);

srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 3. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
setrandvar_buildsample( srvu , srvx );

// 4. Create the chaos polynomial
pc = polychaos_new ( srvx , 1 );
polychaos_setdegree(pc,degre);

// 5. Perform the experiments
np = setrandvar_getsize(srvu);
polychaos_setsizetarget(pc,np);
inputdata = setrandvar_getsample(srvu);
outputdata = ishigami(inputdata);
polychaos_settarget(pc,outputdata);

// 6. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,"Integration");

/////////////////////////////////////////////////////////////////

// Input of the polynomial : in [0,1]
x = [0.15 0.30 0.45];     
// Input of ishigami : in [-pi,pi]
// We could directly set u = (x - 0.5) * 2. * %pi; 
// but the following is more general:
u(1) = randvar_getvalue ( rvu1 , rvx1 , x(1) )
u(2) = randvar_getvalue ( rvu2 , rvx2 , x(2) )
u(3) = randvar_getvalue ( rvu3 , rvx3 , x(3) )

// A first method
polychaos_computeoutput ( pc , x );
yp = polychaos_getoutput ( pc ) ;
ys = ishigami(u);
mprintf("polynome = %e, ishigami = %e\n",yp,ys);

// A second method
polychaos_setinput ( pc , x );
polychaos_computeoutput ( pc );
yp = polychaos_getoutput ( pc ) ;
mprintf("polynome = %e, ishigami = %e\n",yp,ys);

/////////////////////////////////////////////////////////////////

//
// Clean-up
//
randvar_destroy ( rvu1 );
randvar_destroy ( rvu2 );
randvar_destroy ( rvu3 );
randvar_destroy ( rvx1 );
randvar_destroy ( rvx2 );
randvar_destroy ( rvx3 );
setrandvar_destroy ( srvu );
polychaos_destroy ( pc );
setrandvar_destroy ( srvx );
//
// Load this script into the editor
//
filename = 'polychaos_ishigamimetamodel.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

