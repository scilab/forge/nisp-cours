//
// This help file was automatically generated from nisp_expinv.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_expinv.sci
//

// Check the inversion
lambda=2;
x=3;
p = nisp_expcdf ( x , lambda ); // p = 0.9975212
x = nisp_expinv ( p , lambda ) // must be 3
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_expinv.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
