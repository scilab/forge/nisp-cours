//
// This help file was automatically generated from nisp_lognormalcdf.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_lognormalcdf.sci
//

// See http://en.wikipedia.org/wiki/File:Lognormal_distribution_CDF.png
scf();
x = linspace ( 0 , 3 , 1000 );
p = nisp_lognormalcdf ( x , 0.0 , 10 );
plot ( x , p , "k" );
p = nisp_lognormalcdf ( x , 0.0 , 3/2 );
plot ( x , p , "b" );
p = nisp_lognormalcdf ( x , 0.0 , 1 );
plot ( x , p , "g" );
p = nisp_lognormalcdf ( x , 0.0 , 1/2 );
plot ( x , p , "y" );
p = nisp_lognormalcdf ( x , 0.0 , 1/4 );
plot ( x , p , "r" );
legend ( ["s=10" "s=3/2" "s=1" "s=1/2" "s=1/4"] );
xtitle("The log-normale cumulated distribution function","X","P(x)");
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_lognormalcdf.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
