//
// This help file was automatically generated from nisp_buildlhs.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_buildlhs.sci
//

// Create a Lhs design with 5 points in 2 dimensions.
n = 5;
s = 2;
sampling = lhsampling ( s , n );
scf();
plot ( sampling(:,1) , sampling(:,2) , "bo" );
// Add the cuts
cut = linspace ( 0 , 1 , n + 1 );
for i = 1 : n + 1
plot( [cut(i) cut(i)] , [0 1] , "-" )
end
for i = 1 : n + 1
plot( [0 1] , [cut(i) cut(i)] , "-" )
end
halt()   // Press return to continue
 
// Create a Lhs design with 1000 points in 2 dimensions.
n = 1000;
s =  2;
scf();
sampling = lhsampling ( s , n );
plot ( sampling(:,1) , sampling(:,2) , "bo" );
scf();
histplot ( 50 , sampling(:,1));
scf();
histplot ( 50 , sampling(:,2));
halt()   // Press return to continue
 
// Create a Lhs design with 1000 points in 2 dimensions.
// The first variable is normal, the second is uniform.
n = 1000;
s =  2;
scf();
sampling = lhsampling ( s , n );
// First variable is normal
mu = 2;
sigma=3;
P = sampling(:,1);
Q = 1-sampling(:,1);
sampling(:,1)=cdfnor("X",mu*ones(n,1),sigma*ones(n,1),P,Q);
// Second variable is uniform : do nothing
plot ( sampling(:,1) , sampling(:,2) , "bo" );
scf();
histplot ( 50 , sampling(:,1));
scf();
histplot ( 50 , sampling(:,2));
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_buildlhs.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
