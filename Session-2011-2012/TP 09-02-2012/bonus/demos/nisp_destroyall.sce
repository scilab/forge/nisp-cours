//
// This help file was automatically generated from nisp_destroyall.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_destroyall.sci
//

nisp_destroyall ( )
halt()   // Press return to continue
 
// Example of what can go wrong...
// We create a first set of random variables.
srvx = setrandvar_new( 3 );
// We overwrite the first set, with the same variable.
srvx = setrandvar_new( 3 );
// We have lost one setrandvar.
// We could recover it with
setrandvar_tokens()
// To destroy all the lost object, we can as well save
// some time and destroy them all at once.
nisp_destroyall ( )
// There is no more setrandvar in use.
setrandvar_tokens() // Must be []
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_destroyall.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
