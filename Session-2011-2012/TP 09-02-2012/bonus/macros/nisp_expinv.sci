// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function x = nisp_expinv ( p , lambda )
  // Computes the Exponential Quantile.
  //
  // Calling Sequence
  //   x = nisp_expinv ( p , lambda )
  //
  // Parameters
  // p: a matrix of doubles, the probability
  // lambda : a matrix of doubles
  // x: a matrix of doubles
  //
  // Description
  //   This function computes the Exponential Quantile.
  // This function is vectorized but all the input arguments must have the same size.
  // This function might be inaccurate when p is close to 1. 
  // 
  // TODO : improve implementation (check arguments, check inf, nan, etc...)
  //
  // Examples
  // // Check the inversion
  // lambda=2;
  // x=3;
  // p = nisp_expcdf ( x , lambda ); // p = 0.9975212
  // x = nisp_expinv ( p , lambda ) // must be 3
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // Wikipedia, Exponential distribution function, http://en.wikipedia.org/wiki/Exponential_distribution

  x = - log(1-p)./lambda
endfunction

