// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function C = nisp_cov ( x , y )
    // Returns the empirical covariance matrix of x and y.
  //
  // Calling Sequence
  //   C = nisp_cov ( x , y )
  //
  // Parameters
  // x: a n-by-1 matrix of doubles
  // y: a n-by-1 matrix of doubles
  // C: a 2-by-2 matrix of doubles, the empirical covariance
  //
  // Description
  // Returns the empirical covariance matrix, normalized by n-1, 
  // where n is the number of observations.
  // TODO : add the normalization by n
  // TODO : add the C=cov(x) calling sequence
  //
  // Examples
  // x = [1;2];
  // y = [3;4];
  // C = nisp_cov (x,y)
  // expected = [0.5,0.5;0.5,0.5]
  //
  // x = [230;181;165;150;97;192;181;189;172;170];
  // y = [125;99;97;115;120;100;80;90;95;125];
  // expected = [1152.4556,-88.911111;-88.911111,244.26667]
  // C = nisp_cov (x,y)
  //
  // Authors
  // Copyright (C) 2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // "Introduction to probability and statistics for engineers and scientists.", Sheldon Ross

  x=x(:)
  y=y(:)
  n = size(x,"*")
  x=x-mean(x)
  y=y-mean(y)
  C(1,1) = x'*x/(n-1)
  C(1,2) = x'*y/(n-1)
  C(2,1) = C(1,2)
  C(2,2) = y'*y/(n-1)
endfunction

