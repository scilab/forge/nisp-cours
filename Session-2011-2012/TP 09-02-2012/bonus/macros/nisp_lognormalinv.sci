// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function x = nisp_lognormalinv ( p , mu , sigma )
  // Computes the Lognormal quantile.
  //
  // Calling Sequence
  //   x = nisp_lognormalinv ( p , mu , sigma )
  //
  // Parameters
  // p: a matrix of doubles, the probability
  // mu: a matrix of doubles, the mean of the underlying normal variable
  // sigma: a matrix of doubles, the variance of the underlying normal variable
  // x: a matrix of doubles
  //
  // Description
  //   This function computes the Lognormal quantile.
  // This function is vectorized but all the input arguments must have the same size.
  //
  // Examples
  // //
  // // Check the inverse lognormale
  // mu = 1;
  // sigma=10;
  // x=2;
  // p = nisp_lognormalcdf ( x , mu , sigma ); // 0.4877603
  // x = nisp_lognormalinv ( p , mu , sigma ) // Must be 2
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // Dider Pelat, "Bases et méthodes pour le traitement de données", section 8.2.8, "Loi log-normale".
  // Wikipedia, Lognormal probability distribution function, http://en.wikipedia.org/wiki/File:Lognormal_distribution_PDF.png
  // Wikipedia, Lognormal cumulated distribution function, http://en.wikipedia.org/wiki/File:Lognormal_distribution_CDF.png

  c = nisp_erfcinv(2*p)
  x = exp(mu - c*sqrt(2)*sigma)
endfunction

