// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function p = nisp_lognormalpdf ( x , mu , sigma )
  // Computes the Lognormal PDF.
  //
  // Calling Sequence
  //   p = nisp_lognormalpdf ( x , mu , sigma )
  //
  // Parameters
  // x: a matrix of doubles
  // mu: a matrix of doubles, the mean of the underlying normal variable
  // sigma: a matrix of doubles, the variance of the underlying normal variable
  // p: a matrix of doubles, the probability
  //
  // Description
  //   This function computes the Lognormal PDF.
  // This function is vectorized but all the input arguments must have the same size.
  //
  // Examples
  // // See Dider Pelat, "Bases et méthodes pour le traitement de données"
  // scf();
  // x = linspace ( 0 , 5 , 1000 );
  // p = nisp_lognormalpdf ( x , 0.0 , 1.0 );
  // plot ( x , p );
  // xtitle("The log-normale probability distribution function","X","P(x)");
  //
  // // See http://en.wikipedia.org/wiki/File:Lognormal_distribution_PDF.png
  // scf();
  // x = linspace ( 0 , 5 , 1000 );
  // p = nisp_lognormalpdf ( x , 0.0 , 10 );
  // plot ( x , p , "k" );
  // p = nisp_lognormalpdf ( x , 0.0 , 3/2 );
  // plot ( x , p , "b" );
  // p = nisp_lognormalpdf ( x , 0.0 , 1 );
  // plot ( x , p , "g" );
  // p = nisp_lognormalpdf ( x , 0.0 , 1/2 );
  // plot ( x , p , "y" );
  // p = nisp_lognormalpdf ( x , 0.0 , 1/4 );
  // plot ( x , p , "r" );
  // legend ( ["s=10" "s=3/2" "s=1" "s=1/2" "s=1/4"] );
  // xtitle("The log-normale probability distribution function","X","P(x)");
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // Dider Pelat, "Bases et méthodes pour le traitement de données", section 8.2.8, "Loi log-normale".
  // Wikipedia, Lognormal probability distribution function, http://en.wikipedia.org/wiki/File:Lognormal_distribution_PDF.png
  // Wikipedia, Lognormal cumulated distribution function, http://en.wikipedia.org/wiki/File:Lognormal_distribution_CDF.png

  ii = find ( x<=0 )
  p(ii) = 0
  ii = find ( x>0 )
  c = (log(x(ii)) - mu)/sigma
  p(ii) = exp ( -0.5 * c^2 ) ./ ( sigma * x(ii) * sqrt(2*%pi) )
endfunction

