// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//

// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// A demo of the randvar class.
//

mprintf("Test of randvar\n");

mprintf("Creating a Normale variable ...\n")
mu = 1.0;
sigma = 0.5;
vu1 = randvar_new("Normale",mu,sigma);
mprintf("Display the variable ...\n")
randvar_getlog(vu1); 
values = randvar_getvalue(vu1); 
mprintf("Generated number : %f\n", values)
n = 1000;
mprintf("Generate %d numbers...\n",n)
for i=1:n
    values(i) = randvar_getvalue(vu1);
end
computed = mean (values);
mprintf("Mean: %f (expected = %f)\n", ..
computed, mu)
computed = st_deviation (values);
mprintf("St. dev.: %f (expected = %f)\n", ..
computed, sigma)
randvar_destroy(vu1);
//
// Load this script into the editor
//
filename = 'demo_randvar1.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

