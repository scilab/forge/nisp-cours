// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// References
// [1] "About the use of rank transformation in sensitivity
// analysis of model output"
// Andrea Saltelli, Ilya Sobol
// Reliability Engineering and System Safety, 50 (1995)
// 225-239

function y = GSobol ( x , a )
    // x : a nsimu-by-p matrix of doubles, where nsimu is the number of simulations
    // a : a 1-by-p or p-by-1 matrix of doubles, where p is the number of parameters
    if (prod(size(a))<>size(a,"*")) then
        msg = "%s: Wrong size for input argument #%d: %d-by-%d matrix expected.\n"
        error(msprintf(msg,"GSobol",2,1,size(a,"*")))
    end
    a=a(:)'
    p = size(a,"c")
    nsimu=size(x,"r")
    ncolsx=size(x,"c")
    if (p<>ncolsx) then
        msg = "%s: Wrong size for input argument #%d: %d-by-%d matrix expected.\n"
        error(msprintf(msg,"GSobol",1,nsimu,p))
    end
    a=repmat(a,nsimu,1)
    g = (abs(4*x-2)+a)./(1+a)
    y=prod(g,"c")
endfunction

// [1], p 234, Fig. 10
// 
h=scf();
a = [0 9 99]
colors = ["r" "g" "b"]
for i=1:3
    x = linspace(0,1,1000)';
    y=GSobol ( x , a(i) );
    plot(x,y,colors(i))
end
legend(["a=0","a=9","a=99"],"in_lower_right");
xtitle("Function g(x) for different values of a","x","g(x)")
//
// Check E(gi)
a = 12;
[v,err]=intg(0,1,list(GSobol,a))
mprintf("E(gi)=%f (computed)\n",v)
vexp = 1
mprintf("E(gi)=%f (exact)\n",vexp)
//
// Check E(gi^2)
function y=mygsquare(x,a)
    y=GSobol(x,a)^2
endfunction
a = 12;
[v,err]=intg(0,1,list(mygsquare,a))
mprintf("E(gi^2)=%f (computed)\n",v)
vexp = 1 + 1/(3*(1+a)^2)
mprintf("E(gi^2)=%f (exact)\n",vexp)
//
// Compute the mean, the variance
N = 10000;
p=3;
x = grand(N,p,"def");
a = [5 10 15];
mprintf("a:\n")
disp(a)
y = GSobol ( x , a );
scf();
histplot(20,y)
xtitle("GSobol","Y","Frequency")
mu = mean(y);
mprintf("Mean(G)=%f (estimated)\n",mu)
muexp = 1;
mprintf("Mean(G)=%f (exact)\n",muexp)
sigma = variance(y);
mprintf("Variance(G)=%f (estimated)\n",sigma)
sigmaexp = prod(1 + 1 ./(3*(1+a).^2))-1;
mprintf("Variance(G)=%f (exact)\n",sigmaexp)
//
// Compute the first order sensitivity indices
s = nisp_sobolsaFirst ( list(GSobol,a) , p );
mprintf("Estimated Si:\n")
for i=1:p
    mprintf("S(%d)=%f\n",i,s(i))
end
sexpected = 1 ./(3*(1+a).^2)/sigmaexp;
mprintf("Exact Si:\n")
for i=1:p
    mprintf("S(%d)=%f\n",i,sexpected(i))
end
//
// Compute the total sensitivity indices
st = nisp_sobolsaTotal ( list(GSobol,a) , p );
mprintf("Estimated STi:\n")
for i=1:p
    mprintf("ST(%d)=%f\n",i,st(i))
end
// Compute the product, for j different from i.
// To do this, compute all products, then divide by i.
tmp = prod(1 + 1 ./(3*(1+a).^2));
tmp = tmp./(1 + 1 ./(3*(1+a).^2)) - 1;
stexpected = 1 - tmp/sigmaexp;
mprintf("Exact STi:\n")
for i=1:p
    mprintf("ST(%d)=%f\n",i,stexpected(i))
end
